﻿using System.Collections.Generic;
using Appli.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppliTest
{
    [TestClass]
    public class PreparationTest
    {
        Model model = new Model();

        [TestMethod]
        public void GetProductsOfOrderTest()
        {
            string numOrderTest = "2";
            model.GetProductsOfOrder(numOrderTest);
        }

        [TestMethod]
        public void GetAddressTest()
        {
            Product p = new Product
            {
                Wording = "Mon premier article",
                Id = 1,
            };
            model.GetAddresses(p.Id);
        }

        [TestMethod]
        public void DeleteProductsTest()
        {
            List<Product> pList = new List<Product>();
            Product p1 = new Product
            {
                Wording = "Iron Bananas",
                Id = 2,
            };
            pList.Add(p1);
            Product p2 = new Product
            {
                Wording = "Iron Patatas",
                Id = 3
            };
            pList.Add(p2);
            //model.DeleteProducts(pList, p1.Id, 2);
        }
    }
}
