﻿using Appli.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppliTest
{
    [TestClass]
    public class InternalsMovementsTest : BaseTest
    {
        static Position positionTest;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            // Création d'une arborescence
            Depository depotTest = new Depository() { Name = "testDepository", Address = "Ramon", City = "Bergerac", Postcode = "24100" };
            Model.CreateDepository(depotTest);
            Store storeTest = new Store() { Name = "testStore", Depository = depotTest, DepositoryId = depotTest.Id, Lenght = 50, Width = 100 };
            Model.CreateStore(storeTest);
            Path pathTest = new Path() { Name = "A", Lenght = 50, PathWidth = 50, StorageWidth = 50, Store = storeTest, StoreId = storeTest.Id, Unilateral = true };
            Model.CreatePath(pathTest);
            Row rowTest = new Row() { Name = 12, PairSide = false, Path = pathTest, PathId = pathTest.Id, Width = 50 };
            Model.CreateRow(rowTest);
            SupportType supportTest = new SupportType() { Name = "supportTest", Height = 50, Length = 50, Width = 50 };
            Model.CreateSupportType(supportTest);
            positionTest = new Position() { Name = "A", Barcode = Model.HelperGenerateBarcode(), Row = rowTest, RowId = rowTest.Id, SupportType = supportTest };
            Model.CreatePosition(positionTest);
        }

        [TestMethod]
        public void GetMouvementsTest()
        {
            List<String> args = new List<string>();
            for(int i=0; i<10; i++)
            {
                args.Add("");
            }
            Assert.IsFalse(true);
            //TODO Correct the line bellow
            //Assert.AreEqual(0, Model.GetMovements(args).Count);

            Floor floorStockTest = new Floor() { AddressType = Appli.Model.Class.AddressType.Stock, Position = positionTest, PositionId = positionTest.Id, Value = 3 };
            Model.CreateFloor(floorStockTest);
            Floor floorPickingTest = new Floor() { AddressType = Appli.Model.Class.AddressType.Picking, Position = positionTest, PositionId = positionTest.Id, Value = 1 };
            Model.CreateFloor(floorPickingTest);

            Product productTest = new Product() { Wording = "Objet de Test", IsActive = true };
            ProductFloor productFloorTest = new ProductFloor()
            {
                Barcode = Model.HelperGenerateBarcode(),
                CustomsNumber = Model.HelperGenerateBarcode(),
                Product = productTest,
                ProductId = productTest.Id,
                ExpirationDate = DateTime.Now,
                ReceptionDate = DateTime.Now,
                LotNumber = Model.HelperGenerateBarcode(),
                Quantity = 12,
                Stored = false,
                SerialNumber = Model.HelperGenerateBarcode(),
                Floor = floorStockTest,
                FloorId = floorStockTest.Id
            };
            ContextDb.ProductFloors.Add(productFloorTest);
            productTest.ProductFloors.Add(productFloorTest);
            Model.CreateProduct(productTest);

            Assert.IsFalse(true);
            //TODO Correct the line bellow
            //Assert.AreEqual(1, Model.GetMovements(args).Count);
        }
    }
}
