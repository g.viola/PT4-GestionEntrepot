﻿using System;
using Appli.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Appli.Model.Class;
using System.Collections.Generic;
using Appli.Model.Class.Exception;

namespace AppliTest
{
    [TestClass]
    public class ReceptionTest : BaseTest
    {
        static Product _sampleProductPicking = new Product
        {
            Wording = "testRcpPdtPck",
            IsActive = true,
        };
        static Product _sampleProductStock = new Product
        {
            Wording = "testRcpPdtStr",
            IsActive = true,
        };
        static Product _sampleProductWithTraceability = new Product
        {
            Wording = "testRcpPdtTrc",
            IsActive = true,
        };
        static ProductFloor _traceabilityInfo = new ProductFloor
        {
            CustomsNumber = "256",
            LotNumber = "111",
            SerialNumber = "456",
            ReceptionDate = new DateTime(2018, 05, 10),
            ExpirationDate = new DateTime(2018, 12, 24),
            Barcode = Model.HelperGenerateBarcode()
        };

        static Depository receptionDepository;

        static Product toStorePicking;
        static Product toStoreStock;
        static Product toStoreTraceability;

        //Store
        const int nbStore = 2;
        const int storeWidth = 10000;
        const int storeLength = 10000;

        //Path
        const int nbPath = 2;
        const int pathLength = 7000;
        const int pathWidth = 500;
        const int pathStorageWith = 150;

        //Row
        const int nbRow = 6;
        const int rowWidth = 300;

        //Position
        const int nbPosition = 3;
        static SupportType supportType = Model.GetSupportTypes()[0];

        //Packet
        const int nbFloor = 5;
        const int heightLimit = 1;
        const AddressType addressType = AddressType.Picking;

        [ClassInitialize]
        public static void BeforeReceptionTests(TestContext context)
        {
            Model.CreateProduct(_sampleProductPicking);
            Model.CreateProduct(_sampleProductStock);
            Model.CreateProduct(_sampleProductWithTraceability);

            toStorePicking = Model.GetProducts().Find(pdt => pdt.Wording == _sampleProductPicking.Wording);
            toStoreStock = Model.GetProducts().Find(pdt => pdt.Wording == _sampleProductStock.Wording);
            toStoreTraceability = Model.GetProducts().Find(pdt => pdt.Wording == _sampleProductWithTraceability.Wording);

            string depositoryName = "ReceptionTestDepository";

            Model.CreateDepository(new Depository()
            {
                Name = depositoryName,
                Address = "testAddress",
                City = "testCity",
                Postcode = "11111"
            },//Depository
            nbStore, storeWidth, storeLength,//Store
            nbPath, pathLength, pathWidth, pathStorageWith, false,//Path
            nbRow, rowWidth,//Row
            nbPosition, supportType,//Position
            nbFloor, heightLimit, addressType);//Floor

            receptionDepository = Model.GetDepositories().Find(dep => dep.Name == depositoryName);
        }

        [ClassCleanup]
        public static void AfterReceptionTests()
        {
            toStorePicking = Model.GetProductById(toStorePicking.Id);
            toStoreStock = Model.GetProductById(toStoreStock.Id);
            toStoreTraceability = Model.GetProductById(toStoreTraceability.Id);
            List<Product> products = new List<Product>
            {
                toStorePicking,
                toStoreStock,
                toStoreTraceability
            };
            List<ProductFloor> productFloors = new List<ProductFloor>();
            products.ForEach(pdt => productFloors.AddRange(pdt.ProductFloors));
            productFloors.ForEach(pdtFlr => Model.DeleteProductFloor(pdtFlr));
            products.ForEach(pdt => Model.DeleteProduct(pdt));
            Model.DeleteDepository(receptionDepository);
        }

        [TestMethod]
        public void ReceiveProductPickingTest()
        {
            Address storageAddress = Model.ReceiveProduct(toStorePicking, receptionDepository, out string barcode);

            Assert.IsTrue(Model.GetProducts(true, true).Exists(pdt => pdt.ProductFloors.Exists(pdtFlr => pdtFlr.Product.Wording == toStorePicking.Wording
            && pdtFlr.Floor.AddressType == AddressType.Picking && pdtFlr.Stored == false)));
            //check if an association with the test product exists (effectively checking if it was stored) and if it is picking, and ready to be stored

            Assert.IsNotNull(storageAddress);
            //check if the returned address is not null
        }

        [TestMethod]
        public void ReceiveProductNonExistent()
        {
            try
            {
                Product product = new Product()
                {
                    Wording = "Test",
                    IsActive = true
                };
                Model.ReceiveProduct(product, receptionDepository, out string barcode);
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e,typeof(ShowableException));
                Assert.AreEqual("Le produit spécifié n'existe pas dans la base de données.", e.Message);
            }
        }

        [TestMethod]
        public void ReceiveProductWithNonExistentDepository()
        {
            try
            {
                Depository depository = new Depository()
                {
                    Address = "Test",
                    City = "Test",
                    Name = "Test",
                    Postcode = "12345"
                };
                Model.ReceiveProduct(toStorePicking, depository, out string barcode);
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(ShowableException));
                Assert.AreEqual("Le dépôt spécifié n'existe pas dans la bose de données.", e.Message);
            }
        }

        [TestMethod]
        public void ReceiveProductStockTest()
        {
            Address pickingAddress = Model.ReceiveProduct(toStoreStock, receptionDepository, out string barcode);

            Assert.IsTrue(Model.GetProducts(true, true).Exists(pdt => pdt.ProductFloors.Exists(pdtFlr => pdtFlr.Product.Wording == toStoreStock.Wording
            && pdtFlr.Floor.AddressType == AddressType.Picking && pdtFlr.Stored == false)));
            //check if an association with the test product exists (effectively checking if it was stored) and if it is picking, and ready to be stored

            Address storageAddress = Model.ReceiveProduct(toStoreStock, receptionDepository, out barcode);

            Assert.IsTrue(Model.GetProducts(true, true).Exists(pdt => pdt.ProductFloors.Exists(pdtFlr => pdtFlr.Product.Wording == toStoreStock.Wording
            && pdtFlr.Floor.AddressType == AddressType.Stock && pdtFlr.Stored == false)));
            //check if an association with the test product exists (effectively checking if it was stored) and if it is stock, and readay to be stored

            Assert.IsNotNull(pickingAddress);
            Assert.IsNotNull(storageAddress);
            //check if the returned address is not null
        }

        [TestMethod]
        public void ReceiveProductWithTraceabilityTest()
        {
            Address traceabilityAddress = Model.ReceiveProduct(toStoreTraceability, receptionDepository, out string barcode, _traceabilityInfo);

            Assert.IsTrue(Model.GetProducts(true, true).Exists(pdt => pdt.ProductFloors.Exists(pdtFlr => pdtFlr.Product.Wording == toStoreTraceability.Wording && pdtFlr.Stored == false)));
            //check if an association with the test product exists (effectively checking if it was stored)

            Assert.IsTrue(Model.GetProducts(true, true).Exists(pdt => pdt.ProductFloors.Exists(pdtFlr => pdtFlr.CustomsNumber == _traceabilityInfo.CustomsNumber)));
            Assert.IsTrue(Model.GetProducts(true, true).Exists(pdt => pdt.ProductFloors.Exists(pdtFlr => pdtFlr.LotNumber == _traceabilityInfo.LotNumber)));
            Assert.IsTrue(Model.GetProducts(true, true).Exists(pdt => pdt.ProductFloors.Exists(pdtFlr => pdtFlr.SerialNumber == _traceabilityInfo.SerialNumber)));
            Assert.IsTrue(Model.GetProducts(true, true).Exists(pdt => pdt.ProductFloors.Exists(pdtFlr => pdtFlr.ReceptionDate == _traceabilityInfo.ReceptionDate)));
            Assert.IsTrue(Model.GetProducts(true, true).Exists(pdt => pdt.ProductFloors.Exists(pdtFlr => pdtFlr.ExpirationDate == _traceabilityInfo.ExpirationDate)));
            //check if the product's traceabilty info is correct

            Assert.IsNotNull(traceabilityAddress);
            //check if the returned address is not null
        }
    }
}
