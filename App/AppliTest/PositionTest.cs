﻿using System.Collections.Generic;
using System.Linq;
using Appli.Model;
using Appli.Model.Class;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace AppliTest
{
    [TestClass]
    public class PositionTest : BaseTest
    {
        //Depository
        static Depository _depositoryTest;

        //Store
        static Store _storeTest;
        const int NbStore = 2;
        const int StoreWidth = 10000;
        const int StoreLenght = 10000;

        //Path
        static Path _pathTest;
        const int NbPath = 2;
        const int PathLenght = 7000;
        const int PathWidth = 500;
        const int PathStorageWith = 150;

        //Row
        static Row _rowTest;
        const int NbRow = 6;
        const int RowWidth = 300;

        //Position
        const int NbPosition = 3;
        static SupportType _supportType = Model.GetSupportTypes()[0];

        //Packet
        const int NbFloor = 5;
        const int HeightLimit = 1;
        const AddressType AddressType = Appli.Model.Class.AddressType.Picking;

        static int _idToDelete;

        [ClassInitialize]
        public static void StoreInitialize(TestContext testContext)
        {
            //Necessary if you want to try all delete tests one by one.

            _depositoryTest = new Depository() { Name = "forStoreTest", Address = "Ramon", City = "Bergerac", Postcode = "24100" };
            Model.CreateDepository(_depositoryTest);
            _storeTest = new Store() { Depository = _depositoryTest, Lenght = StoreLenght, Name = "Test", Width = StoreWidth };
            Model.CreateStore(_storeTest);
            _pathTest = new Path { Unilateral = false, Lenght = PathLenght, StorageWidth = PathStorageWith, Name = "B", PathWidth = PathWidth, Store = _storeTest, StoreId = _storeTest.Id };                                                                                                     //Path
            Model.CreatePath(_pathTest);
            _rowTest = new Row() { Name = 25, Path = _pathTest, PathId = _pathTest.Id, PairSide = false, Width = RowWidth };
            Model.CreateRow(_rowTest);
            _idToDelete = Model.CreatePosition(new Position() { Barcode = Model.HelperGenerateBarcode(), SupportType = _supportType, Row = _rowTest, RowId = _rowTest.Id, Name = "A" },  //Position                                                            //Position  
                                                                    NbFloor, HeightLimit, AddressType);                                 //Packet


        }

        [TestMethod]
        public void CreatePositionTest()
        {
            string localName = "N";
            UnExistPositionTest(localName);

            int id = Model.CreatePosition(new Position() { Barcode = Model.HelperGenerateBarcode(), Row = _rowTest, RowId = _rowTest.Id, SupportType = _supportType, Name = localName });

            ExistPositionTest(localName);
        }

        [TestMethod]
        public void CreateRowPositionFloorTest()
        {
            string localName = "D";
            UnExistPositionTest(localName);

            int id = Model.CreatePosition(new Position() { Barcode = Model.HelperGenerateBarcode(), Row = _rowTest, RowId = _rowTest.Id, SupportType = _supportType, Name = localName },             //Position
                                                                    NbFloor, HeightLimit, AddressType);                                             //Packet            

            FloorTest(id);

            ExistPositionTest(localName);
        }

        [TestMethod]
        public void UpdatePositionTest()
        {
            Position position = Model.GetPositions()[0];
            string currentName = "Z";
            position.Name = currentName;
            Model.UpdatePosition(position);
            Assert.IsTrue(Model.GetPositions().Find(po => po.Id == position.Id).Name == currentName);
        }

        [TestMethod]
        public void DeletePositionTest()
        {
            Assert.IsTrue(Model.GetPositions().Exists(pos => pos.Id == _idToDelete));
            Position position = Model.GetPositions().Find(pos => pos.Id == _idToDelete);
            Model.DeletePosition(position);
            Assert.IsFalse(Model.GetPositions().Exists(pos => pos.Id == _idToDelete));
            Assert.IsFalse(Model.GetFloors(position).Any());
        }

        private void FloorTest(int id)
        {
            List<Floor> floors = Model.GetFloors(Model.GetPosition(id));
            Assert.IsTrue(floors.Count == NbFloor);
            Assert.IsTrue(floors.Where(floor => floor.Value <= HeightLimit).All(floor => floor.AddressType == AddressType));
            Assert.IsTrue(floors.Where(floor => floor.Value > HeightLimit).All(floor => floor.AddressType == AddressType.Stock));

        }

        private void ExistPositionTest(string name)
        {
            Assert.IsTrue(Model.GetPositions().Exists(pos => pos.Name == name));
        }

        private void UnExistPositionTest(string name)
        {
            Assert.IsFalse(Model.GetPositions().Exists(pos => pos.Name == name));
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            CleanUpAfterStockTest();
        }
    }
}
