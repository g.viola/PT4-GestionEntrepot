﻿using System;
using System.Collections.Generic;
using System.Linq;
using Appli.Model;
using Appli.Model.Class;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppliTest
{
    [TestClass]
    public class DepositoryTest : BaseTest
    {
        const string Name = "depository";

        //Store
        const int NbStore = 2;
        const int StoreWidth = 10000;
        const int StoreLenght = 10000;

        //Path
        const int NbPath = 2;
        const int PathLenght = 7000;
        const int PathWidth = 500;
        const int PathStorageWith = 150;

        //Row
        const int NbRow = 6;
        const int RowWidth = 300;

        //Position
        const int NbPosition = 3;
        static SupportType _supportType = Model.GetSupportTypes()[0];

        //Packet
        const int NbFloor = 5;
        const int HeightLimit = 1;
        const AddressType AddressType = Appli.Model.Class.AddressType.Picking;

        static int _idToDelete;

        [ClassInitialize]
        public static void DepositoryInitialize(TestContext testContext)
        {
            try
            {
                //Necessary if you want to try all delete tests one by one.

                _idToDelete = Model.CreateDepository(new Depository() { Name = "1", Address = "Ramon", City = "Bergerac", Postcode = "24100" },           //Depository
                                                                        NbStore, StoreWidth, StoreLenght,                                   //Store
                                                                        NbPath, PathLenght, PathWidth, PathStorageWith, false,              //Path 
                                                                        NbRow, RowWidth,                                                    //Row   
                                                                        NbPosition, _supportType,                                            //Position  
                                                                        NbFloor, HeightLimit, AddressType);                                 //Packet
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        [TestMethod]
        public void CreateDepositoryTest()
        {
            UnExistDepositoryTest(Name);
            Model.CreateDepository(new Depository() { Name = Name, Address = "Ramon", City = "Bergerac", Postcode = "24100" });
            ExistDepositoryTest(Name);
        }

        [TestMethod]
        public void CreateDepositoryStoreTest()
        {
            string localName = Name + 1;

            UnExistDepositoryTest(localName);

            int id = Model.CreateDepository(new Depository() { Name = localName, Address = "Ramon", City = "Bergerac", Postcode = "24100" }, //Depository
                                                                    NbStore, StoreWidth, StoreLenght);                                       //Store

            StoreTest(id);

            ExistDepositoryTest(localName);
        }

        [TestMethod]
        public void CreateDepositoryStorePathTest()
        {
            bool unilateral = false;
            string localName = Name + 2;
            UnExistDepositoryTest(localName);

            int id = Model.CreateDepository(new Depository() { Name = localName, Address = "Ramon", City = "Bergerac", Postcode = "24100" },        //Depository
                                                                    NbStore, StoreWidth, StoreLenght,                                               //Store
                                                                    NbPath, PathLenght, PathWidth, PathStorageWith, unilateral);                    //Path                                

            StoreTest(id);
            PathTest(id, unilateral);

            ExistDepositoryTest(localName);
        }

        [TestMethod]
        public void CreateDepositoryStorePathRowTest()
        {
            string localName = Name + 3;
            bool unilateral = false;
            UnExistDepositoryTest(localName);

            int id = Model.CreateDepository(new Depository() { Name = localName, Address = "Ramon", City = "Bergerac", Postcode = "24100" },        //Depository
                                                                    NbStore, StoreWidth, StoreLenght,                                               //Store
                                                                    NbPath, PathLenght, PathWidth, PathStorageWith, unilateral,                     //Path 
                                                                    NbRow, RowWidth);                                                               //Row                

            StoreTest(id);
            PathTest(id, unilateral);
            RowTest(id, unilateral);

            ExistDepositoryTest(localName);
        }

        [TestMethod]
        public void CreateDepositoryStorePathUnilateralRowTest()
        {
            bool unilateral = true;
            string localName = Name + 4;
            UnExistDepositoryTest(localName);

            int id = Model.CreateDepository(new Depository() { Name = localName, Address = "Ramon", City = "Bergerac", Postcode = "24100" },             //Depository
                                                                    NbStore, StoreWidth, StoreLenght,                                               //Store
                                                                    NbPath, PathLenght, PathWidth, PathStorageWith, unilateral,                     //Path 
                                                                    NbRow, RowWidth);                                                               //Row                

            StoreTest(id);
            PathTest(id, unilateral);
            RowTest(id, unilateral);

            ExistDepositoryTest(localName);
        }

        [TestMethod]
        public void CreateDepositoryStorePathUnilateralRowPositionTest()
        {
            bool unilateral = true;
            string localName = Name + 5;
            UnExistDepositoryTest(localName);

            int id = Model.CreateDepository(new Depository() { Name = localName, Address = "Ramon", City = "Bergerac", Postcode = "24100" },        //Depository
                                                                    NbStore, StoreWidth, StoreLenght,                                               //Store
                                                                    NbPath, PathLenght, PathWidth, PathStorageWith, unilateral,                     //Path 
                                                                    NbRow, RowWidth,                                                                //Row   
                                                                    NbPosition, _supportType);                                                       //Position        

            StoreTest(id);
            PathTest(id, unilateral);
            RowTest(id, unilateral);
            PositionTest(id);

            ExistDepositoryTest(localName);
        }

        [TestMethod]
        public void CreateDepositoryStorePathUnilateralRowPositionFloorTest()
        {
            bool unilateral = true;
            string localName = Name + 6;
            UnExistDepositoryTest(localName);

            int id = Model.CreateDepository(new Depository() { Name = localName, Address = "Ramon", City = "Bergerac", Postcode = "24100" },        //Depository
                                                                    NbStore, StoreWidth, StoreLenght,                                               //Store
                                                                    NbPath, PathLenght, PathWidth, PathStorageWith, unilateral,                     //Path 
                                                                    NbRow, RowWidth,                                                                //Row   
                                                                    NbPosition, _supportType,                                                        //Position  
                                                                    NbFloor, HeightLimit, AddressType);                                             //Packet            

            StoreTest(id);
            PathTest(id, unilateral);
            RowTest(id, unilateral);
            PositionTest(id);
            FloorTest(id);

            ExistDepositoryTest(localName);
        }

        [TestMethod]
        public void UpdateDepositoryTest()
        {
            Depository depository = Model.GetDepositories()[0];
            string currentName = depository.Name + "Updated";
            depository.Name = currentName;
            Model.UpdateDepository(depository);
            Assert.IsTrue(Model.GetDepositories().Find(dep => dep.Id == depository.Id).Name == currentName);
        }

        [TestMethod]
        public void DeleteDepositoryTest()
        {
            string localName = "1";
            Assert.IsTrue(Model.GetDepositories().Exists(dep => dep.Id == _idToDelete));
            Depository depository = Model.GetDepositories().Find(dep => dep.Id == _idToDelete);
            Model.DeleteDepository(depository);
            Assert.IsFalse(Model.GetDepositories().Exists(dep => dep.Id == _idToDelete));
            Assert.IsFalse(Model.GetStores(depository).Any());
            Assert.IsFalse(Model.GetPaths(depository).Any());
            Assert.IsFalse(Model.GetRows(depository).Any());
            Assert.IsFalse(Model.GetPositions(depository).Any());
            Assert.IsFalse(Model.GetFloors(depository).Any());
        }

        private void StoreTest(int id)
        {
            List<Store> stores = Model.GetStores(Model.GetDepository(id));
            Assert.IsTrue(stores.Count == NbStore);
            Assert.IsTrue(stores.All(store => store.Width == StoreWidth && store.Lenght == StoreLenght));
        }

        private void PathTest(int id, bool unilateral)
        {
            List<Path> paths = Model.GetPaths(Model.GetDepository(id));
            Assert.IsTrue(paths.Count == NbStore * NbPath);
            Assert.IsTrue(paths.All(path => path.Lenght == PathLenght && path.PathWidth == PathWidth && path.StorageWidth == PathStorageWith && path.Unilateral == unilateral));
        }

        private void RowTest(int id, bool unilateral)
        {
            List<Row> rows = Model.GetRows(Model.GetDepository(id));
            Assert.IsTrue(rows.Count == NbStore * NbPath * NbRow);
            Assert.IsTrue(rows.All(row => row.Width == RowWidth));
            if (unilateral)
                Assert.IsTrue(rows.All(row => row.PairSide == false));
            else
                Assert.IsTrue(rows.Any(row => row.PairSide));
        }

        private void PositionTest(int id)
        {
            List<Position> positions = Model.GetPositions(Model.GetDepository(id));
            Assert.IsTrue(positions.Count == NbStore * NbPath * NbRow * NbPosition);
            Assert.IsTrue(positions.All(position => position.SupportType.Id == _supportType.Id));
        }

        private void FloorTest(int id)
        {
            List<Floor> floors = Model.GetFloors(Model.GetDepository(id));
            Assert.IsTrue(floors.Count == NbStore * NbPath * NbRow * NbPosition * NbFloor);
            Assert.IsTrue(floors.Where(floor => floor.Value <= HeightLimit).All(floor => floor.AddressType == AddressType));
            Assert.IsTrue(floors.Where(floor => floor.Value > HeightLimit).All(floor => floor.AddressType == AddressType.Stock));

        }

        private void ExistDepositoryTest(string name)
        {
            Assert.IsTrue(Model.GetDepositories().Exists(dep => dep.Name == name));
        }

        private void UnExistDepositoryTest(string name)
        {
            Assert.IsFalse(Model.GetDepositories().Exists(dep => dep.Name == name));
        }


        [ClassCleanup]
        public static void Cleanup()
        {
            CleanUpAfterStockTest();
        }
    }
}
