﻿using System.Collections.Generic;
using System.Linq;
using Appli.Model;
using Appli.Model.Class;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace AppliTest
{
    [TestClass]
    public class StoreTest : BaseTest
    {
        const string Name = "store";

        //Depository
        static Depository _depositoryTest;

        //Store
        const int NbStore = 2;
        const int StoreWidth = 10000;
        const int StoreLenght = 10000;

        //Path
        const int NbPath = 2;
        const int PathLenght = 7000;
        const int PathWidth = 500;
        const int PathStorageWith = 150;

        //Row
        const int NbRow = 6;
        const int RowWidth = 300;

        //Position
        const int NbPosition = 3;
        static SupportType _supportType = Model.GetSupportTypes()[0];

        //Packet
        const int NbFloor = 5;
        const int HeightLimit = 1;
        const AddressType AddressType = Appli.Model.Class.AddressType.Picking;

        static int _idToDelete;

        [ClassInitialize]
        public static void StoreInitialize(TestContext testContext)
        {
            //Necessary if you want to try all delete tests one by one.

            _depositoryTest = new Depository() { Name = "1", Address = "Ramon", City = "Bergerac", Postcode = "24100" }; 
            Model.CreateDepository(_depositoryTest);                                                                                     //Depository
            _idToDelete = Model.CreateStore(new Store() { Width = StoreWidth, Lenght = StoreLenght, Name = "12", Depository = _depositoryTest }, //Store
                                                                    NbPath, PathLenght, PathWidth, PathStorageWith, false,              //Path 
                                                                    NbRow, RowWidth,                                                    //Row   
                                                                    NbPosition, _supportType,                                            //Position  
                                                                    NbFloor, HeightLimit, AddressType);                                 //Store


        }

        [TestMethod]
        public void CreateStoreTest()
        {
            UnExistStoreTest(Name);
            Model.CreateStore(new Store() { Width = StoreWidth, Lenght = StoreLenght, Depository = _depositoryTest, DepositoryId = _depositoryTest.Id, Name = Name });
            ExistStoreTest(Name);
        }


        [TestMethod]
        public void CreateStorePathTest()
        {
            bool unilateral = false;
            string localName = Name + 2;
            UnExistStoreTest(localName);

            int id = Model.CreateStore(new Store() { Width = StoreWidth, Lenght = StoreLenght, Depository = _depositoryTest, DepositoryId = _depositoryTest.Id, Name = localName },             //Store
                                                                    NbPath, PathLenght, PathWidth, PathStorageWith, unilateral);                    //Path                                

            PathTest(id, unilateral);

            ExistStoreTest(localName);
        }

        [TestMethod]
        public void CreateStorePathRowTest()
        {
            string localName = Name + 3;
            bool unilateral = false;
            UnExistStoreTest(localName);

            int id = Model.CreateStore(new Store() { Width = StoreWidth, Lenght = StoreLenght, Depository = _depositoryTest, DepositoryId = _depositoryTest.Id, Name = localName },             //Depository
                                                                    NbPath, PathLenght, PathWidth, PathStorageWith, unilateral,                     //Path 
                                                                    NbRow, RowWidth);                                                               //Row                

            PathTest(id, unilateral);
            RowTest(id, unilateral);

            ExistStoreTest(localName);
        }

        [TestMethod]
        public void CreateStorePathUnilateralRowTest()
        {
            bool unilateral = true;
            string localName = Name + 4;
            UnExistStoreTest(localName);

            int id = Model.CreateStore(new Store() { Width = StoreWidth, Lenght = StoreLenght, Depository = _depositoryTest, DepositoryId = _depositoryTest.Id, Name = localName },             //Store
                                                                    NbPath, PathLenght, PathWidth, PathStorageWith, unilateral,                     //Path 
                                                                    NbRow, RowWidth);                                                               //Row                

            PathTest(id, unilateral);
            RowTest(id, unilateral);

            ExistStoreTest(localName);
        }

        [TestMethod]
        public void CreateStorePathUnilateralRowPositionTest()
        {
            bool unilateral = true;
            string localName = Name + 5;
            UnExistStoreTest(localName);

            int id = Model.CreateStore(new Store() { Width = StoreWidth, Lenght = StoreLenght, Depository = _depositoryTest, DepositoryId = _depositoryTest.Id, Name = localName },             //Store
                                                                    NbPath, PathLenght, PathWidth, PathStorageWith, unilateral,                     //Path 
                                                                    NbRow, RowWidth,                                                                //Row   
                                                                    NbPosition, _supportType);                                                       //Position        

            PathTest(id, unilateral);
            RowTest(id, unilateral);
            PositionTest(id);

            ExistStoreTest(localName);
        }

        [TestMethod]
        public void CreateStorePathUnilateralRowPositionFloorTest()
        {
            bool unilateral = true;
            string localName = Name + 6;
            UnExistStoreTest(localName);

            int id = Model.CreateStore(new Store() { Width = StoreWidth, Lenght = StoreLenght, Depository = _depositoryTest, DepositoryId = _depositoryTest.Id, Name = localName },             //Store
                                                                    NbPath, PathLenght, PathWidth, PathStorageWith, unilateral,                     //Path 
                                                                    NbRow, RowWidth,                                                                //Row   
                                                                    NbPosition, _supportType,                                                        //Position  
                                                                    NbFloor, HeightLimit, AddressType);                                             //Packet            

            PathTest(id, unilateral);
            RowTest(id, unilateral);
            PositionTest(id);
            FloorTest(id);

            ExistStoreTest(localName);
        }

        [TestMethod]
        public void UpdateStoreTest()
        {
            Store store = Model.GetStores()[0];
            string currentName = store.Name + "Updated";
            store.Name = currentName;
            Model.UpdateStore(store);
            Assert.IsTrue(Model.GetStores().Find(sto => sto.Id == store.Id).Name == currentName);
        }

        [TestMethod]
        public void DeleteStoreTest()
        {
            Assert.IsTrue(Model.GetStores().Exists(str => str.Id == _idToDelete));
            Store store = Model.GetStores().Find(str => str.Id == _idToDelete);
            Model.DeleteStore(store);
            Assert.IsFalse(Model.GetStores().Exists(str => str.Id == _idToDelete));
            Assert.IsFalse(Model.GetPaths(store).Any());
            Assert.IsFalse(Model.GetRows(store).Any());
            Assert.IsFalse(Model.GetPositions(store).Any());
            Assert.IsFalse(Model.GetFloors(store).Any());
        }

        private void PathTest(int id, bool unilateral)
        {
            List<Path> paths = Model.GetPaths(Model.GetStore(id));
            Assert.IsTrue(paths.Count == NbPath);
            Assert.IsTrue(paths.All(path => path.Lenght == PathLenght && path.PathWidth == PathWidth && path.StorageWidth == PathStorageWith && path.Unilateral == unilateral));
        }

        private void RowTest(int id, bool unilateral)
        {
            List<Row> rows = Model.GetRows(Model.GetStore(id));
            Assert.IsTrue(rows.Count == NbPath * NbRow);
            Assert.IsTrue(rows.All(row => row.Width == RowWidth));
            if (unilateral)
                Assert.IsTrue(rows.All(row => row.PairSide == false));
            else
                Assert.IsTrue(rows.Any(row => row.PairSide));
        }

        private void PositionTest(int id)
        {
            List<Position> positions = Model.GetPositions(Model.GetStore(id));
            Assert.IsTrue(positions.Count == NbPath * NbRow * NbPosition);
            Assert.IsTrue(positions.All(position => position.SupportType.Id == _supportType.Id));
        }

        private void FloorTest(int id)
        {
            List<Floor> floors = Model.GetFloors(Model.GetStore(id));
            Assert.IsTrue(floors.Count == NbPath * NbRow * NbPosition * NbFloor);
            Assert.IsTrue(floors.Where(floor => floor.Value <= HeightLimit).All(floor => floor.AddressType == AddressType));
            Assert.IsTrue(floors.Where(floor => floor.Value > HeightLimit).All(floor => floor.AddressType == AddressType.Stock));

        }

        private void ExistStoreTest(string name)
        {
            Assert.IsTrue(Model.GetStores().Exists(dep => dep.Name == name));
        }

        private void UnExistStoreTest(string name)
        {
            Assert.IsFalse(Model.GetStores().Exists(dep => dep.Name == name));
        }


        [ClassCleanup]
        public static void Cleanup()
        {
            CleanUpAfterStockTest();
        }
    }
}
