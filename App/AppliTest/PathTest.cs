﻿using System.Collections.Generic;
using System.Linq;
using Appli.Model;
using Appli.Model.Class;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppliTest
{
    [TestClass]
    public class PathTest : BaseTest
    {
        const char Name = 'A';

        //Depository
        static Depository _depositoryTest;

        //Store
        static Store _storeTest;
        const int NbStore = 2;
        const int StoreWidth = 10000;
        const int StoreLenght = 10000;

        //Path
        const int NbPath = 2;
        const int PathLenght = 7000;
        const int PathWidth = 500;
        const int PathStorageWith = 150;

        //Row
        const int NbRow = 6;
        const int RowWidth = 300;

        //Position
        const int NbPosition = 3;
        static SupportType _supportType = Model.GetSupportTypes()[0];

        //Packet
        const int NbFloor = 5;
        const int HeightLimit = 1;
        const AddressType AddressType = Appli.Model.Class.AddressType.Picking;

        static int _idToDelete;

        [ClassInitialize]
        public static void PathInitialize(TestContext testContext)
        {
                //Necessary if you want to try all delete tests one by one.

                _depositoryTest = new Depository() { Name = "forStoreTest", Address = "Ramon", City = "Bergerac", Postcode = "24100" };
                Model.CreateDepository(_depositoryTest);                                                                                     //Depository
                _storeTest = new Store() { Depository = _depositoryTest, Lenght = StoreLenght, Name = "Test", Width = StoreWidth };
                Model.CreateStore(_storeTest);
                _idToDelete = Model.CreatePath(new Path { Unilateral = false, Lenght = PathLenght, StorageWidth = PathStorageWith, Name = "B", PathWidth = PathWidth, Store = _storeTest, StoreId = _storeTest.Id }, //Path
                                                                        NbRow, RowWidth,                                                    //Row   
                                                                        NbPosition, _supportType,                                            //Position  
                                                                        NbFloor, HeightLimit, AddressType);                                 //Store
            

        }


        [TestMethod]
        public void CreatePathTest()
        {
            bool unilateral = false;
            string localName = "C";
            UnExistPathTest(localName);

            int id = Model.CreatePath(new Path { Unilateral = unilateral, Lenght = PathLenght, StorageWidth = PathStorageWith, Name = localName, PathWidth = PathWidth, Store = _storeTest, StoreId = _storeTest.Id });             //Path                     

            ExistPathTest(localName);
        }

        [TestMethod]
        public void CreatePathRowTest()
        {
            string localName = "D";
            bool unilateral = false;
            UnExistPathTest(localName);

            int id = Model.CreatePath(new Path { Unilateral = unilateral, Lenght = PathLenght, StorageWidth = PathStorageWith, Name = localName, PathWidth = PathWidth, Store = _storeTest, StoreId = _storeTest.Id },             //Path
                                                                    NbRow, RowWidth);                                                               //Row                

            RowTest(id, unilateral);

            ExistPathTest(localName);
        }

        [TestMethod]
        public void CreatePathUnilateralRowTest()
        {
            bool unilateral = true;
            string localName = "E";
            UnExistPathTest(localName);

            int id = Model.CreatePath(new Path { Unilateral = unilateral, Lenght = PathLenght, StorageWidth = PathStorageWith, Name = localName, PathWidth = PathWidth, Store = _storeTest, StoreId = _storeTest.Id },             //Path
                                                                    NbRow, RowWidth);                                                               //Row                

            RowTest(id, unilateral);

            ExistPathTest(localName);
        }

        [TestMethod]
        public void CreatePathUnilateralRowPositionTest()
        {
            bool unilateral = true;
            string localName = "F";
            UnExistPathTest(localName);

            int id = Model.CreatePath(new Path { Unilateral = true, Lenght = PathLenght, StorageWidth = PathStorageWith, Name = localName, PathWidth = PathWidth, Store = _storeTest, StoreId = _storeTest.Id },             //Path
                                                                    NbRow, RowWidth,                                                                //Row   
                                                                    NbPosition, _supportType);                                                       //Position        

            RowTest(id, unilateral);
            PositionTest(id);

            ExistPathTest(localName);
        }

        [TestMethod]
        public void CreatePathUnilateralRowPositionFloorTest()
        {
            bool unilateral = true;
            string localName = "G";
            UnExistPathTest(localName);

            int id = Model.CreatePath(new Path { Unilateral = true, Lenght = PathLenght, StorageWidth = PathStorageWith, Name = localName, PathWidth = PathWidth, Store = _storeTest, StoreId = _storeTest.Id },             //Path
                                                                    NbRow, RowWidth,                                                                //Row   
                                                                    NbPosition, _supportType,                                                        //Position  
                                                                    NbFloor, HeightLimit, AddressType);                                             //Packet            

            RowTest(id, unilateral);
            PositionTest(id);
            FloorTest(id);

            ExistPathTest(localName);
        }

        [TestMethod]
        public void UpdatePathTest()
        {
            Path path = Model.GetPaths()[0];
            string currentName = "X";
            path.Name = currentName;
            Model.UpdatePath(path);
            Assert.IsTrue(Model.GetPaths().Find(pat => pat.Id == path.Id).Name == currentName);
        }

        [TestMethod]
        public void DeleteStoreTest()
        {
            Assert.IsTrue(Model.GetPaths().Exists(pat => pat.Id == _idToDelete));

            Path path = Model.GetPaths().Find(pat => pat.Id == _idToDelete);
            Model.DeletePath(path);
            Assert.IsFalse(Model.GetPaths().Exists(pat => pat.Id == _idToDelete));
            Assert.IsFalse(Model.GetRows(path).Any());
            Assert.IsFalse(Model.GetPositions(path).Any());
            Assert.IsFalse(Model.GetFloors(path).Any());
        }

        private void RowTest(int id, bool unilateral)
        {
            List<Row> rows = Model.GetRows(Model.GetPath(id));
            Assert.IsTrue(rows.Count == NbRow);
            Assert.IsTrue(rows.All(row => row.Width == RowWidth));
            if (unilateral)
                Assert.IsTrue(rows.All(row => row.PairSide == false));
            else
                Assert.IsTrue(rows.Any(row => row.PairSide));
        }

        private void PositionTest(int id)
        {
            List<Position> positions = Model.GetPositions(Model.GetPath(id));
            Assert.IsTrue(positions.Count == NbRow * NbPosition);
            Assert.IsTrue(positions.All(position => position.SupportType.Id == _supportType.Id));
        }

        private void FloorTest(int id)
        {
            List<Floor> floors = Model.GetFloors(Model.GetPath(id));
            Assert.IsTrue(floors.Count == NbRow * NbPosition * NbFloor);
            Assert.IsTrue(floors.Where(floor => floor.Value <= HeightLimit).All(floor => floor.AddressType == AddressType));
            Assert.IsTrue(floors.Where(floor => floor.Value > HeightLimit).All(floor => floor.AddressType == AddressType.Stock));

        }

        private void ExistPathTest(string localName)
        {
            Assert.IsTrue(Model.GetPaths().Exists(pat => pat.Name == localName));
        }

        private void UnExistPathTest(string localName)
        {
            Assert.IsFalse(Model.GetPaths().Exists(pat => pat.Name == localName));
        }


        [ClassCleanup]
        public static void Cleanup()
        {
            CleanUpAfterStockTest();
        }
    }
}
