﻿using System.Collections.Generic;
using System.Linq;
using Appli.Model;
using Appli.Model.Class;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppliTest
{
    [TestClass]
    public class RowTest : BaseTest
    {
        //Depository
        static Depository _depositoryTest;

        //Store
        static Store _storeTest;
        const int NbStore = 2;
        const int StoreWidth = 10000;
        const int StoreLenght = 10000;

        //Path
        static Path _pathTest;
        const int NbPath = 2;
        const int PathLenght = 7000;
        const int PathWidth = 500;
        const int PathStorageWith = 150;

        //Row
        const int NbRow = 6;
        const int RowWidth = 300;

        //Position
        const int NbPosition = 3;
        static SupportType _supportType = Model.GetSupportTypes()[0];

        //Packet
        const int NbFloor = 5;
        const int HeightLimit = 1;
        const AddressType AddressType = Appli.Model.Class.AddressType.Picking;

        static int _idToDelete;

        [ClassInitialize]
        public static void StoreInitialize(TestContext testContext)
        {
            //Necessary if you want to try all delete tests one by one.

            _depositoryTest = new Depository() { Name = "forStoreTest", Address = "Ramon", City = "Bergerac", Postcode = "24100" };
            Model.CreateDepository(_depositoryTest);                                                                                     //Depository
            _storeTest = new Store() { Depository = _depositoryTest, Lenght = StoreLenght, Name = "Test", Width = StoreWidth };
            Model.CreateStore(_storeTest);
            _pathTest = new Path { Unilateral = false, Lenght = PathLenght, StorageWidth = PathStorageWith, Name = "B", PathWidth = PathWidth, Store = _storeTest, StoreId = _storeTest.Id };                                                                                                     //Path
            Model.CreatePath(_pathTest);
            _idToDelete = Model.CreateRow(new Row() { Name = 25, Path = _pathTest, PathId = _pathTest.Id, PairSide = false, Width = RowWidth },                  //Row   
                                                                    NbPosition, _supportType,                                            //Position  
                                                                    NbFloor, HeightLimit, AddressType);                                 //Packet

        }

        [TestCategory("Row")]
        [TestMethod]
        [Priority(3)]
        public void CreateRowTest()
        {
            int localName = 22;
            UnExistRowTest(localName);

            int id = Model.CreateRow(new Row() { Name = localName, PairSide = true, Path = _pathTest, PathId = _pathTest.Id, Width = RowWidth });             //Row                                                            //Row                

            ExistRowTest(localName);
        }

        [TestCategory("Row")]
        [TestMethod]
        [Priority(5)]
        public void CreateRowPositionTest()
        {
            int localName = 23;
            UnExistRowTest(localName);

            int id = Model.CreateRow(new Row() { Name = localName, PairSide = false, Path = _pathTest, PathId = _pathTest.Id, Width = RowWidth },             //Row 
                                                                    NbPosition, _supportType);                                                       //Position        

            PositionTest(id);

            ExistRowTest(localName);
        }

        [TestCategory("Row")]
        [TestMethod]
        [Priority(6)]
        public void CreateRowPositionFloorTest()
        {
            int localName = 99;
            UnExistRowTest(localName);

            int id = Model.CreateRow(new Row() { Name = localName, PairSide = false, Path = _pathTest, PathId = _pathTest.Id, Width = RowWidth },             //Row  
                                                                    NbPosition, _supportType,                                                        //Position  
                                                                    NbFloor, HeightLimit, AddressType);                                             //Packet            

            PositionTest(id);
            FloorTest(id);

            ExistRowTest(localName);
        }

        [TestCategory("Row")]
        [TestMethod]
        [Priority(7)]
        public void UpdateRowTest()
        {
            Row row = Model.GetRows()[0];
            int currentName = row.Name++;
            row.Name = currentName;
            Model.UpdateRow(row);
            Assert.IsTrue(Model.GetRows().Find(ro => ro.Id == row.Id).Name == currentName);
        }

        [TestCategory("Row")]
        [TestMethod]
        [Priority(8)]
        public void DeleteStoreTest()
        {
            Assert.IsTrue(Model.GetRows().Exists(rw => rw.Id == _idToDelete));
            Row row = Model.GetRows().Find(rw => rw.Id == _idToDelete);
            Model.DeleteRow(row);
            Assert.IsFalse(Model.GetRows().Exists(rw => rw.Id == _idToDelete));
            Assert.IsFalse(Model.GetPositions(row).Any());
            Assert.IsFalse(Model.GetFloors(row).Any());
        }

        private void PositionTest(int id)
        {
            List<Position> positions = Model.GetPositions(Model.GetRow(id));
            Assert.IsTrue(positions.Count == NbPosition);
            Assert.IsTrue(positions.All(position => position.SupportType.Id == _supportType.Id));
        }

        private void FloorTest(int id)
        {
            List<Floor> floors = Model.GetFloors(Model.GetRow(id));
            Assert.IsTrue(floors.Count == NbPosition * NbFloor);
            Assert.IsTrue(floors.Where(floor => floor.Value <= HeightLimit).All(floor => floor.AddressType == AddressType));
            Assert.IsTrue(floors.Where(floor => floor.Value > HeightLimit).All(floor => floor.AddressType == AddressType.Stock));

        }

        private void ExistRowTest(int name)
        {
            Assert.IsTrue(Model.GetRows().Exists(row => row.Name == name));
        }

        private void UnExistRowTest(int name)
        {
            Assert.IsFalse(Model.GetRows().Exists(row => row.Name == name));
        }


        [ClassCleanup]
        public static void Cleanup()
        {
            CleanUpAfterStockTest();
        }
    }
}
