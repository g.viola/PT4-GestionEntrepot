﻿using Appli.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppliTest
{
    [TestClass]
    public class StorageTest : BaseTest
    {
        private static List<ProductFloor> ShowStorage;
        private static List<ProductFloor> AllStorage;
        private static ProductFloor productFloorsTest;
        private static Product productTest;
        private static Floor floorTest;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            // Création of tree view
            Depository depotTest = new Depository() { Name = "testDepository", Address = "Ramon", City = "Bergerac", Postcode = "24100" };
            Model.CreateDepository(depotTest);
            Store storeTest = new Store() { Name = "testStore", Depository = depotTest, DepositoryId = depotTest.Id, Lenght = 50, Width = 100 };
            Model.CreateStore(storeTest);
            Path pathTest = new Path() { Name = "A", Lenght = 50, PathWidth = 50, StorageWidth = 50, Store = storeTest, StoreId = storeTest.Id, Unilateral = true };
            Model.CreatePath(pathTest);
            Row rowTest = new Row() { Name = 12, PairSide = false, Path = pathTest, PathId = pathTest.Id, Width = 50 };
            Model.CreateRow(rowTest);
            SupportType supportTest = new SupportType() { Name = "supportTest", Height = 50, Length = 50, Width = 50 };
            Model.CreateSupportType(supportTest);
            Position positionTest = new Position() { Name = "A", Barcode = Model.HelperGenerateBarcode(), Row = rowTest, RowId = rowTest.Id, SupportType = supportTest };
            Model.CreatePosition(positionTest);
            floorTest = new Floor() { AddressType = Appli.Model.Class.AddressType.Stock, Position = positionTest, PositionId = positionTest.Id, Value = 3 };
            Model.CreateFloor(floorTest);

            // Creation of one product
            productTest = new Product() { IsActive = true, Wording = "Produit de test" };

            // Creation of one productFloor which is not stocked
            productFloorsTest = new ProductFloor() { Floor = floorTest, Product = productTest, Quantity = 3, LotNumber = "121", SerialNumber = "212", CustomsNumber = "789", ExpirationDate = DateTime.Now.AddMonths(10), ReceptionDate = DateTime.Now, Barcode = Model.HelperGenerateBarcode() };
            productTest.ProductFloors.Add(productFloorsTest);

            if (!ContextDb.Products.Any(prdt => prdt.Wording == productTest.Wording))
                Model.CreateProduct(productTest);

            ContextDb.ProductFloors.Add(productFloorsTest);

            AllStorage = Model.GetRegisteredProducts();
            ShowStorage = new List<ProductFloor>(AllStorage);
        }

        [Ignore]
        [TestMethod]
        public void DeleteNoStocked()
        {
            Assert.AreEqual(1, ShowStorage.Count);
            //Model.DeleteProductFloorsNoStocked(productFloorsTest.Barcode, productFloorsTest.Floor.Position.Barcode, ShowStorage, AllStorage);
            Assert.AreEqual(0, ShowStorage.Count);
        }

        [ClassCleanup]
        public static void Clean()
        {
            ContextDb.Products.RemoveRange(ContextDb.Products.ToList());
            ContextDb.ProductFloors.RemoveRange(ContextDb.ProductFloors.ToList());
            CleanUpAfterStockTest();
        }
    }
}
