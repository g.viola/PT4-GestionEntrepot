﻿using System;
using System.Drawing;
using System.Linq;
using System.Net.Mime;
using Appli;
using Appli.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppliTest
{
    [TestClass]
    public class HelperTest : BaseTest
    {
        [TestMethod]
        public void HelperGenerateGraphiqueBarcodeTest()
        {
            Image image = Model.HelperGetGraphicBarcode("1111111111111");
            Assert.IsNotNull(image);
        }

        [TestMethod]
        public void HelperGenerateReceptionLabelToPrint()
        {
            Store store = new Store()
            {
                Depository = Model.GetDepositories()[0],
                Lenght = 100000,
                Name = "Test",
                Width = 100000

            };
            Globals.Model.CreateStore(store, 1000, 1000, 1000, 1, false, 1, 1, 50, Model.GetSupportTypes()[0]);
            Product product = new Product()
            {
                IsActive = true,
                Wording = "Coucou !"
            };
            ProductFloor productFloor = new ProductFloor()
            {
                Product = product,
                ReceptionDate = DateTime.Today,
                CustomsNumber = "1234567891234",
                ExpirationDate = DateTime.Today.AddDays(7),
                Floor = Model.GetFloors().First()
            };

            //Model.ReceiveProduct(product, Model.GetDepositories()[0], out string barcode, productFloor);
            //string result = "Fiche produit" +Environment.NewLine + Environment.NewLine;
            //result "Code de l'article : " + productFloor.ProductId);
            //sheet.AppendLine("Libellé : " + productFloor.Product.Wording);
            //if (!string.IsNullOrWhiteSpace(productFloor.CustomsNumber))
            //{
            //    sheet.AppendLine("Quantité : " + productFloor.Quantity);
            //    sheet.AppendLine("Date de réception : " + productFloor.ReceptionDate.Value.ToShortDateString());
            //    sheet.AppendLine("Date de Péremption : " + productFloor.ExpirationDate.Value.ToShortDateString());
            //    sheet.AppendLine("Numéro de série : " + productFloor.SerialNumber);
            //    sheet.AppendLine("Numéro de Douane : " + productFloor.CustomsNumber);
            //    sheet.AppendLine("Numéro de Lot : " + productFloor.LotNumber);
            //}
            //Assert.Equals(Model.HelperGenerateReceptionLabelToPrint(barcode).ToString());
        }
    }
}