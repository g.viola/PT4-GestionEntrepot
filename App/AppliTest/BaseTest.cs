﻿using System;
using System.Linq;
using Appli;
using Appli.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppliTest
{
    [TestClass]
    public class BaseTest
    {
        protected static AppliContext ContextDb = new AppliContext();
        static public Model Model;

        public BaseTest()
        {
        }

        [AssemblyInitialize]
        public static void Initialize(TestContext testContext)
        {
            Model = new Model();
            if (ContextDb.Depositories.Any())
                CleanUp();
            Appli.Migrations.DatabaseInitialization.FillDatabase(ContextDb, true);
            ContextDb.SaveChanges();
        }

        [AssemblyCleanup]
        public static void CleanUp()
        {
            Console.Write("******************** TEST CLEANUP ********************");
            ContextDb.Customers.RemoveRange(ContextDb.Customers.ToList());
            ContextDb.Depositories.RemoveRange(ContextDb.Depositories.ToList());
            ContextDb.Floors.RemoveRange(ContextDb.Floors.ToList());
            ContextDb.OrderProducts.RemoveRange(ContextDb.OrderProducts.ToList());
            ContextDb.Orders.RemoveRange(ContextDb.Orders.ToList());
            ContextDb.PacketArticles.RemoveRange(ContextDb.PacketArticles.ToList());
            ContextDb.Paths.RemoveRange(ContextDb.Paths.ToList());
            ContextDb.Positions.RemoveRange(ContextDb.Positions.ToList());
            ContextDb.ProductFloors.RemoveRange(ContextDb.ProductFloors.ToList());
            ContextDb.Products.RemoveRange(ContextDb.Products.ToList());
            ContextDb.Rows.RemoveRange(ContextDb.Rows.ToList());
            ContextDb.Stores.RemoveRange(ContextDb.Stores.ToList());
            ContextDb.Supplier.RemoveRange(ContextDb.Supplier.ToList());
            ContextDb.SupportTypes.RemoveRange(ContextDb.SupportTypes.ToList());
            ContextDb.SaveChanges();
        }

        protected static void CleanUpAfterStockTest()
        {
            ContextDb.Depositories.RemoveRange(ContextDb.Depositories.ToList());
            ContextDb.Stores.RemoveRange(ContextDb.Stores.ToList());
            ContextDb.Paths.RemoveRange(ContextDb.Paths.ToList());
            ContextDb.Rows.RemoveRange(ContextDb.Rows.ToList());
            ContextDb.Positions.RemoveRange(ContextDb.Positions.ToList());
            ContextDb.Floors.RemoveRange(ContextDb.Floors.ToList());
        }
    }
}
