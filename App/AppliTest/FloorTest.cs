﻿using System.Linq;
using Appli.Model;
using Appli.Model.Class;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppliTest
{
    [TestClass]
    public class FloorTest : BaseTest
    {
        static string _name = "Z";

        //Depository
        static Depository _depositoryTest;

        //Store
        static Store _storeTest;
        const int NbStore = 2;
        const int StoreWidth = 10000;
        const int StoreLenght = 10000;

        //Path
        static Path _pathTest;
        const int NbPath = 2;
        const int PathLenght = 7000;
        const int PathWidth = 500;
        const int PathStorageWith = 150;

        //Row
        static Row _rowTest;
        const int NbRow = 6;
        const int RowWidth = 300;

        //Position
        static Position _positionTest;
        const int NbPosition = 3;
        static SupportType _supportType = Model.GetSupportTypes()[0];

        //Packet
        const int NbFloor = 5;
        const int HeightLimit = 1;
        const AddressType AddressType = Appli.Model.Class.AddressType.Picking;

        static int _idToDelete;

        [ClassInitialize]
        public static void StoreInitialize(TestContext testContext)
        {
            //Necessary if you want to try all delete tests one by one.

            _depositoryTest = new Depository() { Name = "forStoreTest", Address = "Ramon", City = "Bergerac", Postcode = "24100" };
            Model.CreateDepository(_depositoryTest);
            _storeTest = new Store() { Depository = _depositoryTest, Lenght = StoreLenght, Name = "Test", Width = StoreWidth };
            Model.CreateStore(_storeTest);
            _pathTest = new Path { Unilateral = false, Lenght = PathLenght, StorageWidth = PathStorageWith, Name = "B", PathWidth = PathWidth, Store = _storeTest, StoreId = _storeTest.Id };                                                                                                     //Path
            Model.CreatePath(_pathTest);
            _rowTest = new Row() { Name = 25, Path = _pathTest, PathId = _pathTest.Id, PairSide = false, Width = RowWidth };
            Model.CreateRow(_rowTest);
            _positionTest = new Position() { Barcode = Model.HelperGenerateBarcode(), Row = _rowTest, RowId = _rowTest.Id, SupportType = _supportType, Name = _name };                                                                                             //Position
            Model.CreatePosition(_positionTest);
            _idToDelete = Model.CreateFloor(new Floor() { AddressType = AddressType, Value = 1, Position = _positionTest, PositionId = _positionTest.Id });                                //Packet


        }

        [TestMethod]
        public void CreateRowPositionFloorTest()
        {
            int localValue = 2;
            UnExistFloorTest(localValue, _positionTest);

            int id = Model.CreateFloor(new Floor() { AddressType = AddressType, Value = localValue, Position = _positionTest, PositionId = _positionTest.Id });            //Packet            

            ExistFloorTest(localValue, _positionTest);
        }

        [TestMethod]
        public void UpdatePositionTest()
        {
            Floor floor = Model.GetFloors().First(x => x.AddressType != AddressType.Stock);
            Assert.IsTrue(floor.AddressType != AddressType.Stock);
            floor.AddressType = AddressType.Stock;
            Model.UpdateFloor(floor);
            Assert.IsTrue(Model.GetFloors().Find(fl => fl.Id == floor.Id).AddressType == AddressType.Stock);
        }

        [TestMethod]
        public void DeletePositionTest()
        {
            Assert.IsTrue(Model.GetFloors().Exists(fl => fl.Id == _idToDelete));
            Floor floor = Model.GetFloors().Find(fl => fl.Id == _idToDelete);
            Model.DeleteFloor(floor);
            Assert.IsFalse(Model.GetFloors().Exists(fl => fl.Id == _idToDelete));
        }

        private void ExistFloorTest(int value, Position pos)
        {
            Assert.IsTrue(Model.GetFloors().Exists(fl => fl.Value == value && fl.Position.Id == pos.Id));
        }

        private void UnExistFloorTest(int value, Position pos)
        {
            Assert.IsFalse(Model.GetFloors().Exists(fl => fl.Value == value && fl.Position.Id == pos.Id));
        }


        [ClassCleanup]
        public static void Cleanup()
        {
            CleanUpAfterStockTest();
        }
    }
}
