﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Appli.Model
{
    public class CustomerOrder : Order
    {
        /// <summary>
        /// The address of the customer
        /// </summary>
        [Required]
        public string Address { get; set; }

        /// <summary>
        /// The postcode of the customer. It must be 5 numbers
        /// </summary>
        [Required]
        [StringLength(5)]
        [RegularExpression(@"^(?!00000)[0-9]{5,5}$", ErrorMessage = "Code postal invalide")]
        public string Postcode { get; set; }

        /// <summary>
        /// The city of th customer
        /// </summary>
        [Required]
        public string City { get; set; }

        /// <summary>
        /// The customer who pass the order
        /// </summary>
        [ForeignKey(nameof(Customer))]
        public int CustomerId { get; set; }
        [ForeignKey(nameof(CustomerId))]
        public Customer Customer { get; set; }

    }
}
