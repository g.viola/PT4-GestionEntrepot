﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace Appli.Model
{
    [DebuggerDisplay("Id={Id};Name={Name};Address={Address} {Postcode} {City}")]
    public class Supplier
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The name of the supplier
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// The address of the supplier
        /// </summary>
        [Required]
        public string Address { get; set; }

        /// <summary>
        /// The postcode of the supplier. It must be 5 numbers
        /// </summary>
        [Required]
        [StringLength(5)]
        [RegularExpression(@"^(?!00000)[0-9]{5,5}$", ErrorMessage = "Code postal invalide")]
        public string Postcode { get; set; }

        /// <summary>
        /// THe city of the supplier
        /// </summary>
        [Required]
        public string City { get; set; }
    }
}
