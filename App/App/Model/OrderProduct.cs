﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Appli.Model
{
    [DebuggerDisplay("ProductId={ProductId};OrderId={OrderId};Quantity={Quantity}")]
    /// <summary>
    /// Used to link products to an order
    /// </summary>
    public class OrderProduct
    {
        [Key]
        [Column(Order = 1)]
        [ForeignKey(nameof(Product))]
        public int ProductId { get; set; }
        [ForeignKey(nameof(ProductId))]
        public Product Product { get; set; }

        [Key]
        [Column(Order = 0)]
        [ForeignKey(nameof(Order))]
        public int OrderId { get; set; }
        [ForeignKey(nameof(OrderId))]
        public Order Order { get; set; }

        /// <summary>
        /// The quantity of the product in this order
        /// </summary>
        [Required]
        public int Quantity { get; set; }
    }
}
