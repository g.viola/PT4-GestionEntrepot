﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Appli.Model
{
    /// <summary>
    /// Used to link articles to a packet
    /// </summary>
    public class PacketArticle
    {
        [Key]
        [ForeignKey(nameof(Packet))]
        [Column(Order = 0)]
        public int PacketId { get; set; }
        [ForeignKey(nameof(PacketId))]
        public Packet Packet { get; set; }

        [Key]
        [ForeignKey(nameof(Article))]
        [Column(Order = 1)]
        public int ArticlesId { get; set; }
        [ForeignKey(nameof(ArticlesId))]
        public Product Article { get; set; }

        public PacketArticle(Packet packet, Product article)
        {
            Packet = packet;
            Article = article;
        }
        public PacketArticle() { }
        public PacketArticle(int packetId, int articleId)
        {
            PacketId = packetId;
            ArticlesId = articleId;
        }

        [Required]
        public int Quantity { get; set; }
    }
}
