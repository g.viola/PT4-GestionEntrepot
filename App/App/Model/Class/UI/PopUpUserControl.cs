﻿using MaterialDesignThemes.Wpf;


namespace Appli.Model.Class.UI
{
    public class PopUpUserControl : AppliUserControl
    {
        public DialogSession DialogSession { get; set; }

        public virtual void SurOuverture(object sender, DialogOpenedEventArgs eventArgs)
        {
            DialogSession = eventArgs.Session;
        }
    }
}
