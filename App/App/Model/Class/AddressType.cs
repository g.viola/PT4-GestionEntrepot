﻿namespace Appli.Model.Class
{
    public enum AddressType
    {
        Picking,
        Stock,
        PreparedOrder
    }
}
