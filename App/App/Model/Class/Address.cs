﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Appli.Model.Class
{
    public class Address : IEqualityComparer<Address>
    {
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// It is the floor of the address
        /// </summary>
        [Required]
        public Floor Floor { get; set; }

        /// <summary>
        /// It is the position of the address
        /// </summary>
        [Required]
        public Position Position { get; set; }

        /// <summary>
        /// It is the row of the address
        /// </summary>
        [Required]
        public Row Row { get; set; }

        /// <summary>
        /// It is the path of the address
        /// </summary>
        [Required]
        public Path Path { get; set; }

        /// <summary>
        /// It is the store of the address
        /// </summary>
        [Required]
        public Store Store { get; set; }

        /// <summary>
        /// It is the Depository of the address
        /// </summary>
        [Required]
        public Depository Depository { get; set; }

        public Address(Floor floor)
        {
            Floor = floor;
            Position = floor.Position;
            Row = floor.Position.Row;
            Path = floor.Position.Row.Path;
            Store = floor.Position.Row.Path.Store;
            Depository = floor.Position.Row.Path.Store.Depository;
        }

        public string ToStringHuman()
        {
            return Depository.Name + " "
                + Store.Name + " "
                + Path.Name +
                +Row.Name + " "
                + Position.Name +
                +Floor.Value;
        }

        public override string ToString()
        {
            return Depository.Id.ToString() +
                Store.Id.ToString() +
                Path.Id.ToString() +
                Row.Id.ToString() +
                Position.Id.ToString() +
                Floor.Id.ToString();
        }

        bool IEqualityComparer<Address>.Equals(Address x, Address y)
        {
            if (x.Id.Equals(y.Id))
                return true;
            else return false;
        }

        int IEqualityComparer<Address>.GetHashCode(Address obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
