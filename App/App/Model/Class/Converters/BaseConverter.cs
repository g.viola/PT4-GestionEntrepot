﻿using System;
using System.Windows.Markup;

namespace Appli.Model.Class.Converters
{
    public abstract class BaseConverter : MarkupExtension
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
