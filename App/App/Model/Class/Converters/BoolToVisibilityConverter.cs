﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Appli.Model.Class.Converters
{
    public class BoolToVisibilityConverter: BaseConverter, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return Visibility.Collapsed;

            var isVisible = System.Convert.ToBoolean(value);

            return isVisible ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var visiblity = (Visibility)value;

            return visiblity == Visibility.Visible;
        }
    }
}
