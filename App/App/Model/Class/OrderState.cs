﻿namespace Appli.Model.Class
{
    public enum OrderState
    {
        Validated,
        Prepared,
        Delivered
    }
}
