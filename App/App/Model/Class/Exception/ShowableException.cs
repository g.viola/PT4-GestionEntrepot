﻿namespace Appli.Model.Class.Exception
{
    /// <summary>
    /// Used to difference the exception which you can show to the user and others
    /// </summary>
    public class ShowableException : System.Exception
    {
        public ShowableException(string message) : base(message) { }
    }
}
