﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace Appli.Model
{
    [DebuggerDisplay("Id={Id};Name={Name};Address={Address} {Postcode} {City}")]
    public class Depository
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The name of the depository
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// The address of the depository
        /// </summary>
        [Required]
        public string Address { get; set; }

        /// <summary>
        /// The postcode of the depositary. It must be 5 numbers
        /// </summary>
        [Required]
        [StringLength(5)]
        [RegularExpression(@"^(?!00000)[0-9]{5,5}$", ErrorMessage = "Invalide postcode")]
        public string Postcode { get; set; }

        /// <summary>
        /// The city of the depository
        /// </summary>
        [Required]
        public string City { get; set; }

        public Depository(Depository depository)
        {
            Id = depository.Id;
            Name = depository.Name;
            Address = depository.Address;
            Postcode = depository.Postcode;
            City = depository.City;
        }

        public Depository()
        {
        }
    }
}
