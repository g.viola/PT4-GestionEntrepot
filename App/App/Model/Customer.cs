﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace Appli.Model
{
    [DebuggerDisplay("Id={Id};LastName={LastName};FirstName={FirstName}")]
    public class Customer
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The last name of the customer
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// The first name of the customer
        /// </summary>
        [Required]
        public string FirstName { get; set; }

    }
}
