﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace Appli.Model
{
    [DebuggerDisplay("Id={Id};Name={Name};Length={Length};Width={Width};Height={Height}")]
    public class SupportType
    {


        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The name associated with the type of support
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// The width in centimeters
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int Width { get; set; }

        /// <summary>
        /// The lenght in centimeters
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int Length { get; set; }

        /// <summary>
        /// The height in centimeters
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int Height { get; set; }

        public SupportType(SupportType supportType)
        {
            Id = supportType.Id;
            Name = supportType.Name;
            Width = supportType.Width;
            Length = supportType.Length;
            Height = supportType.Height;
        }

        public SupportType() { }
    }
}
