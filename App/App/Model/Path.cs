﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Appli.Model
{
    [DebuggerDisplay("Id={Id};Name={Name};StoreId={StoreId};PathWidth={PathWidth};StorageWidth={StorageWidth};Lenght={Lenght};Unilateral={Unilateral}")]
    public class Path
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Storage unit are at the both side of the path or not
        /// </summary>
        [Required]
        public bool Unilateral { get; set; }

        /// <summary>
        /// Width of the path in centimeters
        /// </summary>
        [Required]
        [Range(0,int.MaxValue)]
        public int PathWidth { get; set; }

        /// <summary>
        /// Width of the storage unit in centimeters
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int StorageWidth { get; set; }

        /// <summary>
        /// Lenght of the path in centimeters
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int Lenght { get; set; }

        /// <summary>
        /// Name associated to the path
        /// </summary>
        [Required]
        [RegularExpression(@"^[A-Z]+$")]
        [MaxLength(1)]
        public string Name { get; set; }

        /// <summary>
        /// The parent store
        /// </summary>
        [Required]
        [ForeignKey(nameof(Store))]
        public int StoreId { get; set; }
        [ForeignKey(nameof(StoreId))]
        public Store Store { get; set; }
    }
}
