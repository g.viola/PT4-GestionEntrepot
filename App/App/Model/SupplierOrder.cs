﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Appli.Model
{
    [DebuggerDisplay("SupplierId={SupplierId};DepositoryId={DepositoryId}")]
    public class SupplierOrder : Order
    {
        /// <summary>
        /// The supplier which you want to order
        /// </summary>
        [ForeignKey(nameof(Supplier))]
        public int SupplierId { get; set; }
        [ForeignKey(nameof(SupplierId))]
        public Supplier Supplier { get; set; }


        /// <summary>
        /// The depository where the order has to be delivered
        /// </summary>
        [ForeignKey(nameof(Depository))]
        public int DepositoryId { get; set; }
        [ForeignKey(nameof(DepositoryId))]
        public Depository Depository { get; set; }
    }
}
