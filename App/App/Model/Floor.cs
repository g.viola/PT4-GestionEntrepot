﻿using Appli.Model.Class;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Appli.Model
{
    [DebuggerDisplay("Id={Id};Value={Value};AddressType={AddressType};PositionId={PositionId}")]
    public class Floor
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// This is the floor value. It begins at 0 a
        /// </summary>
        [Required]
        [Range(0, 4)]
        public int Value { get; set; }

        /// <summary>
        /// This is the type of the address, it can be picking address, stock address or prepared order address
        /// </summary>
        [Required]
        public AddressType AddressType { get; set; }

        /// <summary>
        /// Parent position
        /// </summary>
        [Required]
        [ForeignKey(nameof(Position))]
        public int PositionId { get; set; }
        [ForeignKey(nameof(PositionId))]
        public Position Position { get; set; }
    }
}
