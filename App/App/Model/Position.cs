﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Appli.Model
{
    [DebuggerDisplay("ID={Id};Name={Name};SupportTypeID={SupportType.Id};RowId={RowId};Barcode={Barcode}")]
    public class Position
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The support type of the position. Depends of the pallet size
        /// </summary>
        [Required]
        public SupportType SupportType { get; set; }

        /// <summary>
        /// The name of the position
        /// </summary>
        [Required]
        [RegularExpression(@"^[A-Z]+$")]
        [MaxLength(1)]
        public string Name { get; set; }
        
        /// <summary>
        /// The parent row
        /// </summary>
        [Required]
        [ForeignKey(nameof(Row))]
        public int RowId { get; set; }
        [ForeignKey(nameof(RowId))]
        public Row Row { get; set; }

        /// <summary>
        /// The barcode of the column
        /// </summary>
        [Index(IsUnique = true)]
        [Required]
        [StringLength(13)]
        [RegularExpression(@"^[0-9]+$")]
        public string Barcode { get; set; }
    }
}
