﻿using System.Collections.Generic;

namespace Appli.Model
{
    public class Packet : Product
    {




        public List<PacketArticle> Articles { get; set; }

        public Packet()
        {
            Articles = new List<PacketArticle>();
        }

        public Packet(Packet packet)
        {
            Articles = packet.Articles;
            Id = packet.Id;
            IsActive = packet.IsActive;
            ProductFloors = packet.ProductFloors;
            Wording = packet.Wording;
        }
    }
}
