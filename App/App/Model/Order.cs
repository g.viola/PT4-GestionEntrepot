﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Appli.Model.Class;
using System.ComponentModel.DataAnnotations.Schema;

namespace Appli.Model
{
    public abstract class Order
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The date when the order has been passed
        /// </summary>
        [Required]
        public DateTime OrderDate { get; set; }

        /// <summary>
        /// The list of the products that the order contains
        /// </summary>
        List<Product> Products { get; set; }

        /// <summary>
        /// The barcode of the column
        /// </summary>
        [Index(IsUnique = true)]
        [Required]
        [StringLength(13)]
        [RegularExpression(@"^[0-9]+$")]
        public string Barcode { get; set; }

        [Required]
        public OrderState Prepared { get; set; }      
    }
}
