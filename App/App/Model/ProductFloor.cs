﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Appli.Model
{
    [DebuggerDisplay("ProductId={ProductId};FloorId={FloorId};Quantity={Quantity};LotNumber={LotNumber};CustomsNumber={CustomsNumber};SerialNumber={SerialNumber};ExpirationDate={ExpirationDate};ReceptionDate={ReceptionDate};")]
    /// <summary>
    /// Used to link floors to a product
    /// </summary>
    public class ProductFloor
    {
        [Key]
        [Column(Order = 0)]
        [ForeignKey(nameof(Product))]
        public int ProductId { get; set; }
        [ForeignKey(nameof(ProductId))]
        public Product Product { get; set; }

        [Key]
        [Column(Order = 1)]
        [ForeignKey(nameof(Floor))]
        public int FloorId { get; set; }
        [ForeignKey(nameof(FloorId))]
        public Floor Floor { get; set; }

        /// <summary>
        /// The quantity of this product that contains a floor
        /// </summary>
        [Required]
        public int Quantity { get; set; }

        /// <summary>
        /// The number of the lot
        /// </summary>
        [Display(Name = "Numéro de lot")]
        [MaxLength(13)]
        public string LotNumber { get; set; }

        /// <summary>
        /// The customs number
        /// </summary>
        [Display(Name = "Numéro de douane")]
        [MaxLength(13)]
        public string CustomsNumber { get; set; }

        /// <summary>
        /// The serial number
        /// </summary>
        [Display(Name = "Numéro de série")]
        [MaxLength(13)]
        public string SerialNumber { get; set; }

        /// <summary>
        /// The expiry date
        /// </summary>
        [Display(Name = "Date de péremption")]
        public DateTime? ExpirationDate { get; set; }

        /// <summary>
        /// The receipt date
        /// </summary>
        [Display(Name = "Date de réception")]
        public DateTime? ReceptionDate { get; set; }

        /// <summary>
        /// Whether the specified product is stored at the specified position
        /// </summary>
        public bool Stored { get; set; }
        /// <summary>
        /// The barcode of the productfloor
        /// </summary>
        [Index(IsUnique = true)]
        [Required]
        [StringLength(13)]
        [RegularExpression(@"^[0-9]+$")]
        public string Barcode { get; set; }
    }
}

