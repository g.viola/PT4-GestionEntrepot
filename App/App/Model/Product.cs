﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Appli.Model
{
    //[DebuggerDisplay("Id={Id};Wording={Wording};IsActive={IsActive}};ProductFloors={ProductFloors.Count}")]
    public class Product
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The commercial name of the product
        /// </summary>
        [Display(Name = "Libellé")]
        [Required]
        [Index(IsUnique = true)]
        [MaxLength(150)]
        public string Wording { get; set; }

        /// <summary>
        /// All completes address of the product
        /// </summary>
        public List<ProductFloor> ProductFloors { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public string IdWording => Id + " - " + Wording;

        public Product()
        {
            ProductFloors = new List<ProductFloor>();
        }

        public Product(Product product)
        {
            this.Id = product.Id;
            IsActive = product.IsActive;
            ProductFloors = product.ProductFloors;
            Wording = product.Wording;
        }


    }
}
