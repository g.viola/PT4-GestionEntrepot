namespace Appli.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialization : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LastName = c.String(nullable: false),
                        FirstName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Depositories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        Postcode = c.String(nullable: false, maxLength: 5),
                        City = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Floors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        AddressType = c.Int(nullable: false),
                        PositionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Positions", t => t.PositionId)
                .Index(t => t.PositionId);
            
            CreateTable(
                "dbo.Positions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 1),
                        RowId = c.Int(nullable: false),
                        Barcode = c.String(nullable: false, maxLength: 13),
                        SupportType_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rows", t => t.RowId)
                .ForeignKey("dbo.SupportTypes", t => t.SupportType_Id)
                .Index(t => t.RowId)
                .Index(t => t.Barcode, unique: true)
                .Index(t => t.SupportType_Id);
            
            CreateTable(
                "dbo.Rows",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Width = c.Int(nullable: false),
                        Name = c.Int(nullable: false),
                        PathId = c.Int(nullable: false),
                        PairSide = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Paths", t => t.PathId)
                .Index(t => t.PathId);
            
            CreateTable(
                "dbo.Paths",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Unilateral = c.Boolean(nullable: false),
                        PathWidth = c.Int(nullable: false),
                        StorageWidth = c.Int(nullable: false),
                        Lenght = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 1),
                        StoreId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Stores", t => t.StoreId)
                .Index(t => t.StoreId);
            
            CreateTable(
                "dbo.Stores",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        DepositoryId = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Lenght = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Depositories", t => t.DepositoryId)
                .Index(t => t.DepositoryId);
            
            CreateTable(
                "dbo.SupportTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Width = c.Int(nullable: false),
                        Length = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderProducts",
                c => new
                    {
                        OrderId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.OrderId, t.ProductId })
                .ForeignKey("dbo.Orders", t => t.OrderId)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .Index(t => t.OrderId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderDate = c.DateTime(nullable: false),
                        Barcode = c.String(nullable: false, maxLength: 13),
                        Prepared = c.Int(nullable: false),
                        Address = c.String(),
                        Postcode = c.String(maxLength: 5),
                        City = c.String(),
                        CustomerId = c.Int(),
                        SupplierId = c.Int(),
                        DepositoryId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerId)
                .ForeignKey("dbo.Depositories", t => t.DepositoryId)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId)
                .Index(t => t.Barcode, unique: true)
                .Index(t => t.CustomerId)
                .Index(t => t.SupplierId)
                .Index(t => t.DepositoryId);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        Postcode = c.String(nullable: false, maxLength: 5),
                        City = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Wording = c.String(nullable: false, maxLength: 150),
                        IsActive = c.Boolean(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Wording, unique: true);
            
            CreateTable(
                "dbo.ProductFloors",
                c => new
                    {
                        ProductId = c.Int(nullable: false),
                        FloorId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        LotNumber = c.String(maxLength: 13),
                        CustomsNumber = c.String(maxLength: 13),
                        SerialNumber = c.String(maxLength: 13),
                        ExpirationDate = c.DateTime(),
                        ReceptionDate = c.DateTime(),
                        Stored = c.Boolean(nullable: false),
                        Barcode = c.String(nullable: false, maxLength: 13),
                    })
                .PrimaryKey(t => new { t.ProductId, t.FloorId })
                .ForeignKey("dbo.Floors", t => t.FloorId)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .Index(t => t.ProductId)
                .Index(t => t.FloorId)
                .Index(t => t.Barcode, unique: true);
            
            CreateTable(
                "dbo.PacketArticles",
                c => new
                    {
                        PacketId = c.Int(nullable: false),
                        ArticlesId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PacketId, t.ArticlesId })
                .ForeignKey("dbo.Products", t => t.ArticlesId)
                .ForeignKey("dbo.Products", t => t.PacketId)
                .Index(t => t.PacketId)
                .Index(t => t.ArticlesId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderProducts", "ProductId", "dbo.Products");
            DropForeignKey("dbo.PacketArticles", "PacketId", "dbo.Products");
            DropForeignKey("dbo.PacketArticles", "ArticlesId", "dbo.Products");
            DropForeignKey("dbo.ProductFloors", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductFloors", "FloorId", "dbo.Floors");
            DropForeignKey("dbo.OrderProducts", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.Orders", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.Orders", "DepositoryId", "dbo.Depositories");
            DropForeignKey("dbo.Orders", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Floors", "PositionId", "dbo.Positions");
            DropForeignKey("dbo.Positions", "SupportType_Id", "dbo.SupportTypes");
            DropForeignKey("dbo.Positions", "RowId", "dbo.Rows");
            DropForeignKey("dbo.Rows", "PathId", "dbo.Paths");
            DropForeignKey("dbo.Paths", "StoreId", "dbo.Stores");
            DropForeignKey("dbo.Stores", "DepositoryId", "dbo.Depositories");
            DropIndex("dbo.PacketArticles", new[] { "ArticlesId" });
            DropIndex("dbo.PacketArticles", new[] { "PacketId" });
            DropIndex("dbo.ProductFloors", new[] { "Barcode" });
            DropIndex("dbo.ProductFloors", new[] { "FloorId" });
            DropIndex("dbo.ProductFloors", new[] { "ProductId" });
            DropIndex("dbo.Products", new[] { "Wording" });
            DropIndex("dbo.Orders", new[] { "DepositoryId" });
            DropIndex("dbo.Orders", new[] { "SupplierId" });
            DropIndex("dbo.Orders", new[] { "CustomerId" });
            DropIndex("dbo.Orders", new[] { "Barcode" });
            DropIndex("dbo.OrderProducts", new[] { "ProductId" });
            DropIndex("dbo.OrderProducts", new[] { "OrderId" });
            DropIndex("dbo.Stores", new[] { "DepositoryId" });
            DropIndex("dbo.Paths", new[] { "StoreId" });
            DropIndex("dbo.Rows", new[] { "PathId" });
            DropIndex("dbo.Positions", new[] { "SupportType_Id" });
            DropIndex("dbo.Positions", new[] { "Barcode" });
            DropIndex("dbo.Positions", new[] { "RowId" });
            DropIndex("dbo.Floors", new[] { "PositionId" });
            DropTable("dbo.PacketArticles");
            DropTable("dbo.ProductFloors");
            DropTable("dbo.Products");
            DropTable("dbo.Suppliers");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderProducts");
            DropTable("dbo.SupportTypes");
            DropTable("dbo.Stores");
            DropTable("dbo.Paths");
            DropTable("dbo.Rows");
            DropTable("dbo.Positions");
            DropTable("dbo.Floors");
            DropTable("dbo.Depositories");
            DropTable("dbo.Customers");
        }
    }
}
