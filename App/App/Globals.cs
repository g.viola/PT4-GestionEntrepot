﻿using Appli.Model.Class.UI;
using MaterialDesignThemes.Wpf;
using System;
using System.Threading.Tasks;
using System.Windows;

namespace Appli
{
    public static class Globals
    {
        public static Model.Model Model;

        public static MainWindow MainWindow
        {
            get { return ((MainWindow) Application.Current.MainWindow); }
        }

        public static async void OpenDialog(PopUpUserControl control, DialogClosingEventHandler handlerClosing, object tagValue = null)
        {
            Task task = null;
            var dialogHost = MainWindow.RootDialog;
            MainWindow.Activate();
            if (!dialogHost.IsOpen)
            {
                dialogHost.Tag = tagValue;
                task = DialogHost.Show(control, dialogHost.Identifier, control.SurOuverture, handlerClosing);
            }

            if (task.IsFaulted)
            {
                MessageBox.Show(string.Join(Environment.NewLine, task.Exception), "Erreurs", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            if (task == null)
                return;
            await task;
        }
    }
}
