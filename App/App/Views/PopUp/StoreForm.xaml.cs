﻿using Appli.Model;
using Appli.Model.Class.Exception;
using System.Windows;

namespace Appli.Views.PopUp
{
    /// <summary>
    /// Interaction logic for DepositoryForm.xaml
    /// </summary>
    public partial class StoreForm
    {
        private Store _store;

        public Store Store
        {
            get { return _store; }
            set
            {
                if (value == _store) return;
                _store = value;
                OnPropertyChanged();
            }
        }

        public StoreForm(Store store)
        {
            Store = store;
            InitializeComponent();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (!DialogSession.IsEnded)
                DialogSession.Close("Aucune modification n'a été enregistré");
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Globals.Model.UpdateStore(Store);
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Voulez vous vraiment supprimer ce magasin ?", "Attention", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Globals.Model.DeleteStore(Store);
                }
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }
        }
    }
}
