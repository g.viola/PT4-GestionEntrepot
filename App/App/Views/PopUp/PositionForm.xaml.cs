﻿using Appli.Model;
using Appli.Model.Class.Exception;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Appli.Views.PopUp
{
    /// <summary>
    /// Interaction logic for DepositoryForm.xaml
    /// </summary>
    public partial class PositionForm
    {
        private Position _position;

        public Position Position
        {
            get { return _position; }
            set
            {
                if (value == _position) return;
                _position = value;
                OnPropertyChanged();
            }
        }

        private List<SupportType> _supportTypes;

        public List<SupportType> SupportTypes
        {
            get { return _supportTypes; }
            set
            {
                if (value == _supportTypes) return;
                _supportTypes = value;
                OnPropertyChanged();
            }
        }


        public PositionForm(Position position)
        {
            Position = position;
            SupportTypes = Globals.Model.GetSupportTypes();
            InitializeComponent();
            PositionSupportType.SelectedItem = SupportTypes.SingleOrDefault(support => support.Id == position.SupportType.Id);
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (!DialogSession.IsEnded)
                DialogSession.Close("Aucune modification n'a été enregistré");
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Position.SupportType = (SupportType) PositionSupportType.SelectedItem;
                Globals.Model.UpdatePosition(Position);
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;

            }

        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Voulez vous vraiment supprimer cette position ?", "Attention",
                        MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;

                Globals.Model.DeletePosition(Position);
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }
        }
    }
}
