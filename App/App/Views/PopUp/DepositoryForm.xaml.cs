﻿using Appli.Model;
using Appli.Model.Class.Exception;
using System.Windows;

namespace Appli.Views.PopUp
{
    /// <summary>
    /// Interaction logic for DepositoryForm.xaml
    /// </summary>
    public partial class DepositoryForm
    {
        private Depository _depository;

        public Depository Depository
        {
            get { return _depository; }
            set
            {
                if (_depository == value) return;
                _depository = value;
                OnPropertyChanged();
            }
        }

        public bool Creation { get; set; }

        public DepositoryForm(Depository depository)
        {
            Depository = new Depository(depository);
            InitializeComponent();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (!DialogSession.IsEnded)
                DialogSession.Close("Aucune modification n'a été enregistré");
        }


        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                Globals.Model.UpdateDepository(Depository);
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }


        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Voulez vous vraiment supprimer ce dépôt ?", "Attention", MessageBoxButton.YesNo,
                        MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;
                Globals.Model.DeleteDepository(Depository);
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }
        }
    }
}
