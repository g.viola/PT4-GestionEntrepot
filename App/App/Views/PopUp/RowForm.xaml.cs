﻿using Appli.Model;
using Appli.Model.Class.Exception;
using System.Windows;

namespace Appli.Views.PopUp
{
    /// <summary>
    /// Interaction logic for DepositoryForm.xaml
    /// </summary>
    public partial class RowForm
    {
        private Row _row;

        public Row Row
        {
            get { return _row; }
            set
            {
                if (value == _row) return;
                _row = value;
                OnPropertyChanged();
            }
        }

        public RowForm(Row row)
        {
            Row = row;
            InitializeComponent();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (!DialogSession.IsEnded)
                DialogSession.Close("Aucune modification n'a été enregistré");
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Globals.Model.UpdateRow(Row);
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }

        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Voulez vous vraiment supprimer ce rang ?", "Attention", MessageBoxButton.YesNo,
                        MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;

                Globals.Model.DeleteRow(Row);
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }
        }
    }
}
