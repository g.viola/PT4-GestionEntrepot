﻿using Appli.Model;
using Appli.Model.Class;
using Appli.Model.Class.Exception;
using System.Windows;

namespace Appli.Views.PopUp
{
    /// <summary>
    /// Interaction logic for DepositoryForm.xaml
    /// </summary>
    public partial class FloorForm
    {
        private Floor _floor;

        public Floor Floor
        {
            get { return _floor; }
            set
            {
                if (value == _floor) return;
                _floor = value;
                OnPropertyChanged();
            }
        }

        public FloorForm(Floor floor)
        {
            Floor = floor;
            InitializeComponent();
            FloorSupportType.SelectedIndex = (int)Floor.AddressType;

        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (!DialogSession.IsEnded)
                DialogSession.Close("Aucune modification n'a été enregistré");
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            Floor.AddressType = (AddressType)FloorSupportType.SelectedIndex;
            try
            {
                Globals.Model.UpdateFloor(Floor);
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }

        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Voulez vous vraiment supprimer cette position ?", "Attention",
                        MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;
                Globals.Model.DeleteFloor(Floor);
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }
        }
    }
}
