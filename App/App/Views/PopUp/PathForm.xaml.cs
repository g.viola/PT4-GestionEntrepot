﻿using Appli.Model.Class.Exception;
using System.Windows;

namespace Appli.Views.PopUp
{
    /// <summary>
    /// Interaction logic for DepositoryForm.xaml
    /// </summary>
    public partial class PathForm
    {
        private Model.Path _path;

        public Model.Path Path
        {
            get { return _path; }
            set
            {
                if (value == _path) return;
                _path = value;
                OnPropertyChanged();
            }
        }

        public PathForm(Model.Path path)
        {
            Path = path;
            InitializeComponent();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (!DialogSession.IsEnded)
                DialogSession.Close("Aucune modification n'a été enregistré");
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Globals.Model.UpdatePath(Path);
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }

        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Voulez vous vraiment supprimer cette allée ?", "Attention", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Globals.Model.DeletePath(Path);
                    if (!DialogSession.IsEnded)
                        DialogSession.Close();
                }
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }
        }
    }
}
