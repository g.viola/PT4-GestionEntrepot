﻿using Appli.Model;
using Appli.Model.Class.Exception;
using System.Windows;

namespace Appli.Views.PopUp
{
    /// <summary>
    /// Interaction logic for DepositoryForm.xaml
    /// </summary>
    public partial class SupportTypeForm
    {
        private SupportType _supportType;

        public SupportType SupportType
        {
            get { return _supportType; }
            set
            {
                if (value == _supportType) return;
                _supportType = value;
                OnPropertyChanged();
            }
        }

        private bool _creation;
        public SupportTypeForm(SupportType supportType = null)
        {
            _creation = supportType == null;
            SupportType = _creation ? new SupportType() : new SupportType(supportType);
            InitializeComponent();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (!DialogSession.IsEnded)
                DialogSession.Close("Aucune modification n'a été enregistré");
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_creation)
                    Globals.Model.CreateSupportType(SupportType);
                else
                    Globals.Model.UpdateSupportType(SupportType);
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;

            }

        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (MessageBox.Show("Voulez vous vraiment supprimer ce type de support ?", "Attention",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;
                Globals.Model.DeleteSupportType(SupportType);
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }
        }
    }
}
