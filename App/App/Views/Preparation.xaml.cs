﻿using Appli.Model;
using Appli.Model.Class;
using Appli.Model.Class.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using Appli.Model.Class.Exception;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Appli.Views
{
    /// <summary>
    /// Interaction logic for Articles.xaml
    /// </summary>
    public partial class Preparation
    {
        /// <summary>
        /// Order which is not prepared
        /// </summary>
        private CustomerOrder _order;
        public CustomerOrder Order
        {
            get { return _order; }
            set
            {
                if (value == _order) return;
                _order = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// List of the productfloors showed in the DataGridVIew
        /// </summary>
        private List<ProductFloor> _productFloorsShowable;

        /// <summary>
        /// Getter and Setter of _productFloorsShowable.
        /// </summary>
        public List<ProductFloor> ProductFloorsShowable
        {
            get { return _productFloorsShowable; }
            set
            {
                if (value == _productFloorsShowable) return;
                _productFloorsShowable = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// List of the possible ProductFloors which will maybe used for the preparation. 
        /// </summary>
        private List<ProductFloor> _productFloors;

        /// <summary>
        /// Getter and Setter of _productFloors.
        /// </summary>
        public List<ProductFloor> ProductFloors
        {
            get { return _productFloors; }
            set
            {
                if (value == _productFloors) return;
                _productFloors = value;
                OnPropertyChanged();
            }
        }
        

        /// <summary>
        /// Constructor of Preparation
        /// </summary>
        public Preparation()
        {
            InitializeComponent();
            ProductFloors = new List<ProductFloor>();
        }



        /// <summary>
        /// Search the product in the specified order.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Search_Click(object sender, RoutedEventArgs e)
        {
            ProductFloors.Clear();
            try
            {
                if (FilterValidation(CbCommande.Name, CbCommande.Text, MessageStart))
                {
                    FillGrid();
                }
            }
            catch (FormatException)
            {
                MessageStart.Text = "Entrer un code barre de commande !";
            }
            GridPreparation.Items.Refresh();
        }

        public void FillGrid()
        {
            bool articlesAvailables = true;
            Product productNotAvailable = null;
            List<OrderProduct> orderProducts = new List<OrderProduct>();
            ProductFloors = new List<ProductFloor>();
            if (Order == null)
                orderProducts = Globals.Model.GetOrderProducts().Where(op => op.Order.Barcode == CbCommande.Text).ToList();
            else
            {
                orderProducts = Globals.Model.GetOrderProducts().Where(op => op.Order.Id == Order.Id).ToList();
            }
            foreach (OrderProduct op in orderProducts)
            {
                ProductFloor productFloor = Globals.Model.GetProductFloor(op.Product, new List<ProductFloor>());
                if (productFloor == null)
                {
                    articlesAvailables = false;
                    productNotAvailable = op.Product;
                    break;
                }
                else
                {
                    int qtt = productFloor.Quantity;
                    do
                    {
                        ProductFloors.Add(productFloor);
                        productFloor = Globals.Model.GetProductFloor(op.Product, ProductFloors);
                        if (productFloor != null)
                            qtt += productFloor.Quantity;
                    }
                    while (qtt < op.Quantity && productFloor != null);
                }

            }
            if (articlesAvailables)
            {
                MessageStart.Text = "";
                ProductFloorsShowable = new List<ProductFloor>(ProductFloors);
                ProductFloorsShowable.OrderBy(x => x.Floor.Position.Row.Path.Store.Depository.Name)
                .ThenBy(x => x.Floor.Position.Row.Path.Store.Name)
                .ThenBy(x => x.Floor.Position.Row.Path.Name)
                .ThenBy(x => x.Floor.Position.Name)
                .ThenBy(x => x.Floor.Id);
            }
            else
                MessageStart.Text = "Il n'y a plus de " + productNotAvailable.Wording + " en picking.";
        }

        /// <summary>
        /// Called when one produt of the order is collected. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Validate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FilterValidation(CbArticle.Name, CbArticle.Text, MessageOk) &&
                    FilterValidation(CbAddress.Name, CbAddress.Text, MessageOk))
                {
                    String codebarreProduit = CbArticle.Text;
                    String codebarreAddress = CbAddress.Text;
                    ProductFloor productFloor = ProductFloorsShowable.FirstOrDefault(pf => pf.Barcode == codebarreProduit && pf.Floor.Position.Barcode == codebarreAddress);
                    if (productFloor != null)
                    {
                        OrderProduct orderProduct = Globals.Model.GetOrderProducts().FirstOrDefault(op => op.Order.Barcode == CbCommande.Text && op.Product.Id == productFloor.Product.Id);
                        if (orderProduct.Quantity >= productFloor.Quantity)
                        {
                            productFloor.Quantity = 0;
                            Globals.Model.UpdateProductFloor(productFloor);
                            Globals.Model.DeleteProductFloor(productFloor);
                            ProductFloorsShowable.Remove(productFloor);
                            Globals.Model.DeactivateProduct(productFloor.Product);
                            orderProduct.Quantity -= productFloor.Quantity;
                            if (orderProduct.Quantity > productFloor.Quantity)
                                Globals.Model.UpdateOrderProduct(orderProduct);
                            else
                                Globals.Model.DeleteOrderProduct(orderProduct);
                        }
                        else if (orderProduct.Quantity < productFloor.Quantity)
                        {
                            productFloor.Quantity -= orderProduct.Quantity;
                            Globals.Model.UpdateProductFloor(productFloor);
                            Globals.Model.DeleteOrderProduct(orderProduct);
                        }
                    }
                    else
                        MessageOk.Text = "Les codes barre ne correspondent pas.";
                }
            }
            catch (FormatException)
            {
                MessageOk.Text = "Entrer un code barre d'adresse et de produit !";
            }
            catch (ArgumentNullException)
            {
                MessageOk.Text = "Ce code barre d'adresse et/ou de produit n'est pas présent dans la commande!";
            }
            catch (NullReferenceException)
            {
                MessageOk.Text = "Ce code barre d'adresse et/ou de produit n'est pas présent dans la commande!";
            }
            Order = Globals.Model.GetNextOrder();
            GridPreparation.Items.Refresh();
        }

        private void BtnPrint_OnClick(object sender, RoutedEventArgs e)
        {
            StringBuilder sheet = Globals.Model.HelperGenerateOrderToPrint(ProductFloors);
            if (sheet == null)
            {
                throw new ShowableException(
                    "Erreur lors de la génération du document.");
            }

            SaveFileDialog saveFile = new SaveFileDialog() { Filter = "PDF file|*.pdf", ValidateNames = true };
            {
                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    Document document = new Document(PageSize.A4.Rotate());
                    FileStream fileStream = new FileStream(saveFile.FileName, FileMode.Create);
                    try
                    {

                        PdfWriter.GetInstance(document, fileStream);
                        document.Open();
                        document.Add(new Paragraph(sheet.ToString()));
                        Image pic = Image.GetInstance(Globals.Model.HelperGetGraphicBarcode(Order.Barcode),
                            System.Drawing.Imaging.ImageFormat.Jpeg);
                        document.Add(pic);
                        document.Add(new Paragraph(Order.Barcode));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw new ShowableException("Erreur lors de la génération du document.");
                    }
                    finally
                    {
                        document.Close();
                        fileStream.Close();
                    }
                    Process.Start(saveFile.FileName);
                }
            }
        }

        private void BtnNextOrder_OnClick(object sender, RoutedEventArgs e)
        {
            Order = Globals.Model.GetNextOrder(Order);
            FillGrid();
        }
    }
}
