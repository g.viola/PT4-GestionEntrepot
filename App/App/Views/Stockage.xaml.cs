﻿using Appli.Model;
using Appli.Model.Class.UI;
using System.Collections.Generic;
using System.Windows;

namespace Appli.Views
{
    /// <summary>
    /// Interaction logic for Stockage.xaml
    /// </summary>
    public partial class Stockage : AppliUserControl
    {

        private List<Product> _produits;
        string _cbProduct;

        public List<Product> Products
        {
            get => _produits;
            set
            {
                if (value == _produits) return;
                _produits = value;
                OnPropertyChanged();
            }
        }

        public Stockage()
        {
            InitializeComponent();
            Products = Globals.Model.GetProducts();
        }

        private void validate_Click(object sender, RoutedEventArgs e)
        {
            if(FilterValidation(labelCBProduct.Content.ToString(), CBProduct.Text, errorMessage))
            {
                _cbProduct = CBProduct.Text;
                errorMessage.Text = "";
            }
        }
    }
}
