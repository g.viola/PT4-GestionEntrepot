﻿using Appli.Model;
using Appli.Model.Class;
using Appli.Model.Class.UI;
using Appli.Views.PopUp;
using MaterialDesignThemes.Wpf;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Appli.Views
{
    /// <summary>
    /// Logique d'interaction pour Settings.xaml
    /// </summary>
    public partial class Configurations
    {

        private List<SupportType> _supportTypes;

        public List<SupportType> SupportTypes
        {
            get
            {
                return _supportTypes;
            }

            set
            {
                if (value == _supportTypes) return;
                _supportTypes = value;
                OnPropertyChanged();
            }
        }

        private List<Path> _paths { get; set; }
        public List<Path> Paths
        {
            get { return _paths; }
            set
            {
                if (value == _paths) return;
                _paths = value;
                OnPropertyChanged();
            }
        }

        private List<Depository> _depositories;

        public List<Depository> Depositories
        {
            get { return _depositories; }
            set
            {
                if (value == _depositories) return;
                _depositories = value;
                OnPropertyChanged();
            }
        }

        private List<Store> _stores;

        public List<Store> Stores
        {
            get { return _stores; }
            set
            {
                if (value == _stores) return;
                _stores = value;
                OnPropertyChanged();
            }
        }

        private List<Row> _rows;

        public List<Row> Rows
        {
            get { return _rows; }
            set
            {
                if (value == _rows) return;
                _rows = value;
                OnPropertyChanged();
            }
        }

        private List<Position> _positions;

        public List<Position> Positions
        {
            get { return _positions; }
            set
            {
                if (value == _positions) return;
                _positions = value;
                OnPropertyChanged();
            }
        }

        private List<Floor> _floors;

        public List<Floor> Floors
        {
            get { return _floors; }
            set
            {
                if (value == _floors) return;
                _floors = value;
                OnPropertyChanged();
            }
        }

        private List<Store> _storesOfDepository;

        public List<Store> StoresOfDepository
        {
            get { return _storesOfDepository; }
            set
            {
                if (value == _storesOfDepository) return;
                _storesOfDepository = value;
                OnPropertyChanged();
            }
        }

        private List<Path> _pathsOfStore;

        public List<Path> PathsOfStore
        {
            get { return _pathsOfStore; }
            set
            {
                if (value == _pathsOfStore) return;
                _pathsOfStore = value;
                OnPropertyChanged();
            }
        }

        private List<Row> _rowsOfPath;

        public List<Row> RowsOfPath
        {
            get { return _rowsOfPath; }
            set
            {
                if (value == _rowsOfPath) return;
                _rowsOfPath = value;
                OnPropertyChanged();
            }
        }

        private List<Position> _positionsOfRow;

        public List<Position> PositionsOfRow
        {
            get { return _positionsOfRow; }
            set
            {
                if (value == _positionsOfRow) return;
                _positionsOfRow = value;
                OnPropertyChanged();
            }
        }

        private List<Floor> _floorsOfPosition;

        public List<Floor> FloorsOfPosition
        {
            get { return _floorsOfPosition; }
            set
            {
                if (value == _floorsOfPosition) return;
                _floorsOfPosition = value;
                OnPropertyChanged();
            }
        }

        public Configurations()
        {
            Depositories = Globals.Model.GetDepositories();
            Stores = Globals.Model.GetStores();
            SupportTypes = Globals.Model.GetSupportTypes();
            Paths = Globals.Model.GetPaths();
            Rows = Globals.Model.GetRows();
            Positions = Globals.Model.GetPositions();
            Floors = Globals.Model.GetFloors();
            InitializeComponent();
            UpdateListOf();
        }

        private void UpdateListOf()
        {
            if (ComboDepositories.SelectedIndex < 0)
                ComboDepositories.SelectedIndex = 0;
            StoresOfDepository = Stores.Where(x => x.DepositoryId == ((Depository)ComboDepositories.SelectedItem).Id).ToList();
            if (ComboStores.SelectedIndex < 0)
                ComboStores.SelectedIndex = 0;
            PathsOfStore = Paths.Where(x => x.StoreId == ((Store)ComboStores.SelectedItem).Id).ToList();
            if (ComboPaths.SelectedIndex < 0)
                ComboPaths.SelectedIndex = 0;
            RowsOfPath = Rows.Where(x => x.PathId == ((Path)ComboPaths.SelectedItem).Id).ToList();
            if (ComboRows.SelectedIndex < 0)
                ComboRows.SelectedIndex = 0;
            PositionsOfRow = Positions.Where(x => x.RowId == ((Row)ComboRows.SelectedItem).Id).ToList();
            if (ComboPositions.SelectedIndex < 0)
                ComboPositions.SelectedIndex = 0;
            FloorsOfPosition = Floors.Where(x => x.PositionId == ((Position)ComboPositions.SelectedItem).Id).ToList();

        }

        private void Combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsInitialized)
            {
                UpdateListOf();
            }
        }

        private void ModificationFormClose(object sender, DialogClosingEventArgs eventArgs)
        {
            if (eventArgs.Parameter != null)
            {
                SnackbarValidation.Background = Brushes.OrangeRed;
                SnackbarValidation.MessageQueue.Enqueue(eventArgs.Parameter);
            }
            else
            {
                SnackbarValidation.Background = Brushes.LawnGreen;
                SnackbarValidation.MessageQueue.Enqueue("Les modifications ont bien été enregistrées.");
            }
            var content = eventArgs.Session.Content;
            if (content.GetType() == typeof(DepositoryForm))
            {
                Depositories = Globals.Model.GetDepositories();
            }
            else if (eventArgs.Session.Content.GetType() == typeof(StoreForm))
            {
                Stores = Globals.Model.GetStores();
            }
            else if (eventArgs.Session.Content.GetType() == typeof(PathForm))
            {
                Paths = Globals.Model.GetPaths();
            }
            else if (eventArgs.Session.Content.GetType() == typeof(RowForm))
            {
                Rows = Globals.Model.GetRows();
            }
            else if (eventArgs.Session.Content.GetType() == typeof(PositionForm))
            {
                Positions = Globals.Model.GetPositions();
            }
            else if (eventArgs.Session.Content.GetType() == typeof(FloorForm))
            {
                Floors = Globals.Model.GetFloors();
            }
            else if (eventArgs.Session.Content.GetType() == typeof(SupportTypeForm))
            {
                SupportTypes = Globals.Model.GetSupportTypes();
            }
        }

        #region Depository
        private void DepositoryGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DepositoryForm depositoryForm = new DepositoryForm((Depository)((DataGridRow)sender).DataContext);
            Globals.OpenDialog(depositoryForm, ModificationFormClose);
        }

        #endregion

        #region Store

        private void StoreGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            StoreForm storeForm = new StoreForm((Store)((DataGridRow)sender).DataContext);
            Globals.OpenDialog(storeForm, ModificationFormClose);
        }

        #endregion

        #region Path
        private void PathGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PathForm pathForm = new PathForm((Path)((DataGridRow)sender).DataContext);
            Globals.OpenDialog(pathForm, ModificationFormClose);
        }

        #endregion

        #region Row
        private void RowGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            RowForm rowForm = new RowForm((Row)((DataGridRow)sender).DataContext);
            Globals.OpenDialog(rowForm, ModificationFormClose);
        }

        #endregion

        #region Position
        private void PositionGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PositionForm positionForm = new PositionForm((Position)((DataGridRow)sender).DataContext);
            Globals.OpenDialog(positionForm, ModificationFormClose);
        }

        #endregion

        #region Packet
        private void FloorGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            FloorForm floorForm = new FloorForm((Floor)((DataGridRow)sender).DataContext);
            Globals.OpenDialog(floorForm, ModificationFormClose);
        }

        #endregion

        #region SupportType
        private void SupportTypeGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            SupportTypeForm supportTypeForm = new SupportTypeForm((SupportType)((DataGridRow)sender).DataContext);
            Globals.OpenDialog(supportTypeForm, ModificationFormClose);
        }

        private void BtnCreateSupportType_Click(object sender, RoutedEventArgs e)
        {
            SupportTypeForm supportTypeForm = new SupportTypeForm();
            Globals.OpenDialog(supportTypeForm, ModificationFormClose);
        }
        #endregion


    }
}
