﻿using Appli.Model;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Appli
{
    public class AppliContext : DbContext
    {

        public AppliContext() : base("ConnectionDB") { }

        public AppliContext(string connectionString) : base(connectionString) { }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<Depository> Depositories { get; set; }

        public DbSet<Supplier> Supplier { get; set; }

        public DbSet<Store> Stores { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Path> Paths { get; set; }

        public DbSet<Row> Rows { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Floor> Floors { get; set; }

        public DbSet<ProductFloor> ProductFloors { get; set; }

        public DbSet<OrderProduct> OrderProducts { get; set; }

        public DbSet<SupportType> SupportTypes { get; set; }

        public DbSet<PacketArticle> PacketArticles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            Database.SetInitializer<AppliContext>(null);
            base.OnModelCreating(modelBuilder);
        }

    }
}
