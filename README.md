# Logiciel de gestion d'entrepôt
## Présentation

Logiciel de gestion d'entrepôts réalisé en C# WPF dans le cadre du projet tutoré du semestre 4 de DUT Informatique à l'IUT de Bordeaux. Réalisé par une équipe de 7 personnes dont moi-même.

## Prérequis
Pour tester l'application il faut :
* Avoir Visual Studio 2015 ou 2017
* Avoir Microsoft SQL SERVER Management Studio 2014 ou 2017 (ou au moins SQL Server 2016 Express LocalDB).

## Utilisation

* Lancer la solution App.sln avec Visual Studio
* Pour créer la base de données, il faut, depuis Visual Studio aller dans Outil > Gestionnaire de paquet Nugget > Console du gestionnaire de paquet.
Dans la console, entrer la commande "Update-Database -ConnectionStringName ConnectionDB" et "Update-Database -ConnectionStringName ConnectionTestDB" pour la base de test, en prenant soin de sélectionner le projet Appli en projet par défaut.
* C'est tout, vous pouvez lancer l'application avec Visual Studio.

## Test

Pour tester le remplissages des codes barres, il peut être utile d'utiliser ces requêtes SQL :
* Pour récupérer les codes barres correspondant à un ProductFloor soit un produit et son adresse de Stockage voici la requête : `Select Positions.Barcode, ProductFloors.Barcode
From ProductFloors Inner Join Floors On ProductFloors.FloorId = Floors.Id
Inner Join Positions On Floors.PositionId = Positions.Id
Inner Join Rows On Positions.RowId = Rows.Id
Inner Join Paths On Rows.PathId = Paths.Id
Inner Join Stores On Paths.StoreId = Stores.Id
Inner Join Depositories On Stores.DepositoryId = Depositories.Id
Where Depositories.Name = 'Depot A' AND Stores.Name = 'Magasin 0' AND Paths.Name = 'A'
AND Rows.Name = 1 AND Positions.Name = 'B' AND Floors.Value = 1;`. Evidemment il faut adapter cette requête avec les bons noms.
* Pour récupérer les codes barres d'un emplacement qui n'est pas encore occupé comme dans le cas de l'adresse finale des mouvements internes voici la requête : `Select Positions.Barcode
From Floors
Inner Join Positions On Floors.PositionId = Positions.Id
Inner Join Rows On Positions.RowId = Rows.Id
Inner Join Paths On Rows.PathId = Paths.Id
Inner Join Stores On Paths.StoreId = Stores.Id
Inner Join Depositories On Stores.DepositoryId = Depositories.Id
Where Depositories.Name = 'Depot A' AND Stores.Name = 'Magasin 0' AND Paths.Name = 'A'
AND Rows.Name = 2 AND Positions.Name = 'B' AND Floors.Value = 0;`
